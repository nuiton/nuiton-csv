package org.nuiton.csv;

/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2013 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class Utf8WithBomTest {

    private static final Log log = LogFactory.getLog(Utf8WithBomTest.class);

    protected ImportModel<ObjectUtils.Null> importModel = new ImportModel<ObjectUtils.Null>() {

        @Override
        public char getSeparator() {
            return ';';
        }

        @Override
        public void pushCsvHeaderNames(List<String> headerNames) {
            // do nothing
        }

        @Override
        public ObjectUtils.Null newEmptyInstance() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Iterable<ImportableColumn<ObjectUtils.Null, Object>> getColumnsForImport() {
            ModelBuilder<ObjectUtils.Null> builder = new ModelBuilder<>();

            builder.newMandatoryColumn("N° 1", "whatever");
            builder.newMandatoryColumn("N° 2", "whatever");
            builder.newMandatoryColumn("Nom", "whatever");

            return (Iterable) builder.getColumnsForImport();
        }

    };

    @Test
    public void testImport() {

        InputStream csv = getClass().getResourceAsStream("/csv-file-encoded-in-UTF-8-with-BOM.csv");

        Import<ObjectUtils.Null> csvImport = Import.newImport(importModel, csv);

        try {
            csvImport.prepareAndValidate();
        } catch (ImportRuntimeException e) {
            if (log.isDebugEnabled()) {
                log.debug("exception caught", e);
            }
            Assert.fail("no exception should have been raised");
        }

    }

    @Test
    public void testImport2() {

        InputStream csv = getClass().getResourceAsStream("/csv-file-encoded-in-UTF-8-with-BOM.csv");

        Import2<ObjectUtils.Null> csvImport = Import2.newImport(importModel, csv);

        try {
            csvImport.prepareAndValidate();
        } catch (ImportRuntimeException e) {
            if (log.isDebugEnabled()) {
                log.debug("exception caught", e);
            }
            Assert.fail("no exception should have been raised");
        }

    }
}
