package org.nuiton.csv;

/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;
import org.nuiton.csv.ext.AbstractImportModel;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Tests the {@link Import2}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.3
 */
public class Import2Test {

    protected ImportModel<RowBean> importModel;

    @Before
    public void setUp() {
        AbstractImportModel<RowBean> importModel = new AbstractImportModel<RowBean>(';') {
            @Override
            public RowBean newEmptyInstance() {
                return new RowBean();
            }
        };
        importModel.newMandatoryColumn("NUMBER", "number", Common.INTEGER);
        importModel.newMandatoryColumn("TITLE", "title", Common.STRING);
        importModel.newMandatoryColumn("DATE", "date", new Common.DateValue("yyyy-MM-dd"));
        importModel.newMandatoryColumn("ROWBEANENUM", "rowBeanEnum", Common.newEnumByNameParserFormatter(RowBeanEnum.class));

        this.importModel = importModel;
    }

    @Test
    public void testSimpleImportWithNoData() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM";

        List<RowBean> rows;
        rows = importContent(importModel, content);
        Assert.assertEquals(0, rows.size());

        content = "DATE;NUMBER;TITLE;ROWBEANENUM\n";
        rows = importContent(importModel, content);
        Assert.assertEquals(0, rows.size());
    }

    @Test
    public void testSimpleImportWithOneLine() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM\n2011-12-05;18;\"1ère ligne\";ZERO;";

        List<RowBean> rows = importContent(importModel, content);
        Assert.assertEquals(1, rows.size());
        assertRowEquals(rows.get(0), DateUtil.createDate(5, 12, 2011), 18, "1ère ligne", RowBeanEnum.ZERO);
    }

    @Test
    public void testSimpleImport() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM\n" +
                         "2011-12-05;18;\"1ère ligne\";ZERO\n" +
                         "2011-12-06;19;\"2ème ligne\";ONE\n" +
                         "2011-12-07;21;\"3ème ligne\";TWO";
        List<RowBean> rows = importContent(importModel, content);
        assertRowEquals(rows.get(0), DateUtil.createDate(5, 12, 2011), 18, "1ère ligne", RowBeanEnum.ZERO);
        assertRowEquals(rows.get(1), DateUtil.createDate(6, 12, 2011), 19, "2ème ligne", RowBeanEnum.ONE);
        assertRowEquals(rows.get(2), DateUtil.createDate(7, 12, 2011), 21, "3ème ligne", RowBeanEnum.TWO);
    }

    @Test
    public void testSimpleImportWithExtraUnknownHEaders() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM;BLABLA\n" +
                         "2011-12-05;18;\"1ère ligne\";ZERO;BLABLA\n" +
                         "2011-12-06;19;\"2ème ligne\";ONE;BLABLA\n" +
                         "2011-12-07;21;\"3ème ligne\";TWO;BLABLA";
        ImportConf conf = new ImportConf();
        conf.setIgnoreUnknownHeader(true);
        List<RowBean> rows = importContent(conf, importModel, content);
        assertRowEquals(rows.get(0), DateUtil.createDate(5, 12, 2011), 18, "1ère ligne", RowBeanEnum.ZERO);
        assertRowEquals(rows.get(1), DateUtil.createDate(6, 12, 2011), 19, "2ème ligne", RowBeanEnum.ONE);
        assertRowEquals(rows.get(2), DateUtil.createDate(7, 12, 2011), 21, "3ème ligne", RowBeanEnum.TWO);
    }

    @Test
    public void testSimpleImportWithNotStrictMode() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM\n" +
                         "2011-12-05;18;\"1ère ligne\";BOUH!\n" +
                         "NOT_A_DATE;19;\"2ème ligne\";ONE\n" +
                         "2011-12-07;AHAH;\"3ème ligne\";TWO";
        ImportConf conf = new ImportConf();
        conf.setStrictMode(false);
        List<RowBean> rows = importContent(conf, importModel, content);
        assertRowEquals(rows.get(0), DateUtil.createDate(5, 12, 2011), 18, "1ère ligne", null);
        assertRowEquals(rows.get(1), null, 19, "2ème ligne", RowBeanEnum.ONE);
        assertRowEquals(rows.get(2), DateUtil.createDate(7, 12, 2011), null, "3ème ligne", RowBeanEnum.TWO);
    }

    @Test
    public void testCaptureRawRecord() {
        String row1 = "2011-12-05;18;\"1ère ligne\";ZERO";
        String row2 = "2011-12-06;19;\"2ème ligne\";ONE";
        String row3 = "2011-12-07;21;\"3ème ligne\";TWO";
        String row4 = ";;";
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM\n" + Joiner.on("\n").join(row1, row2, row3, row4);

        ImportConf conf = new ImportConf();
        conf.setCaptureRawRecord(true);
        conf.setCloneImportRow(true);

        try (
                StringReader reader = new StringReader(content);
                Import2<RowBean> rowImport = Import2.newImport(conf, importModel, reader)
        ) {
            ImmutableList<ImportRow<RowBean>> rows = ImmutableList.copyOf(rowImport);

            Assert.assertEquals(4, rows.size());
            Assert.assertTrue(rows.get(0).getRawRecord().isPresent());
            Assert.assertEquals(row1, rows.get(0).getRawRecord().get());
            Assert.assertTrue(rows.get(1).getRawRecord().isPresent());
            Assert.assertEquals(row2, rows.get(1).getRawRecord().get());
            Assert.assertTrue(rows.get(2).getRawRecord().isPresent());
            Assert.assertEquals(row3, rows.get(2).getRawRecord().get());
            Assert.assertTrue(rows.get(3).getRawRecord().isPresent());
            Assert.assertEquals(row4, rows.get(3).getRawRecord().get());
        }
    }

    @Test
    public void testDontCaptureRawRecord() {
        String row1 = "2011-12-05;18;\"1ère ligne\";ZERO";
        String row2 = "2011-12-06;19;\"2ème ligne\";ONE";
        String row3 = "2011-12-07;21;\"3ème ligne\";TWO";
        String row4 = ";;";
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM\n" + Joiner.on("\n").join(row1, row2, row3, row4);

        ImportConf conf = new ImportConf();
        conf.setCaptureRawRecord(false);
        conf.setCloneImportRow(true);


        try (
                StringReader reader = new StringReader(content);
                Import2<RowBean> rowImport = Import2.newImport(conf, importModel, reader)
        ) {
            ImmutableList<ImportRow<RowBean>> rows = ImmutableList.copyOf(rowImport);

            Assert.assertEquals(4, rows.size());
            Assert.assertFalse(rows.get(0).getRawRecord().isPresent());
            Assert.assertFalse(rows.get(1).getRawRecord().isPresent());
            Assert.assertFalse(rows.get(2).getRawRecord().isPresent());
            Assert.assertFalse(rows.get(3).getRawRecord().isPresent());
        }
    }

    protected List<RowBean> importContent(ImportConf conf,
                                          ImportModel<RowBean> model,
                                          String content) throws IOException {

        try (
                Reader reader = new StringReader(content);
                Import2<RowBean> rowImport = Import2.newImport(conf, model, reader)
        ){
            List<RowBean> result = new ArrayList<>();
            for (ImportRow<RowBean> row : rowImport) {
                result.add(row.getBean());
            }
            return result;
    }
    }

    protected List<RowBean> importContent(ImportModel<RowBean> model,
                                          String content) throws IOException {
        try (
                Reader reader = new StringReader(content);
                Import2<RowBean> rowImport = Import2.newImport(model, reader)
        ){
            List<RowBean> result = new ArrayList<>();
            for (ImportRow<RowBean> row : rowImport) {
                result.add(row.getBean());
            }
            return result;
        }
    }

    private void assertRowEquals(RowBean row, Date date, Integer number, String title, RowBeanEnum rowBeanEnum) {
        Assert.assertEquals(date, row.getDate());
        Assert.assertEquals(number, row.getNumber());
        Assert.assertEquals(title, row.getTitle());
        Assert.assertEquals(rowBeanEnum, row.getRowBeanEnum());
    }
}
