package org.nuiton.csv;

/*-
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2013 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 09/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ExporterTest {

    public static final Charset CHARSET = Charset.forName("UTF-8");

    public static final Iterable<String> RESULTS = ImmutableList.of(
            "\"D~ATE\"~TITLEé~NUMBER~title~rowBeanEnum--\n",
            "\"D~ATE\"~TITLEé~NUMBER~title~rowBeanEnum--\n" +
            "01/12/2011~Batéman~1~Batéman~ONE--\n",
            "\"D~ATE\"~TITLEé~NUMBER~title~rowBeanEnum--\n" +
            "01/12/2011~Batéman~1~Batéman~ONE--\n"          +
            "02/12/2011~~7~~TWO--\n",
            "\"D~ATE\"~TITLEé~NUMBER~title~rowBeanEnum--\n" +
            "01/12/2011~Batéman~1~Batéman~ONE--\n"          +
            "02/12/2011~~7~~TWO--\n"                        +
            "07/12/2011~~9~~ZERO--\n"                       +
            "18/12/2011~~18~~ONE--\n"                       +
            "23/12/2011~~4~~TWO--\n"
    );

    protected static final Iterable<Set<RowBean>> SETS;

    static {

        ImmutableSet.Builder<RowBean> builder = ImmutableSet.builder();

        ImmutableList.Builder<Set<RowBean>> setBuilder = ImmutableList.builder();

        setBuilder.add(Collections.<RowBean>emptySet());

        builder.add(new RowBean(DateUtil.createDate(1, 12, 2011), "Batéman", 1, RowBeanEnum.ONE));
        setBuilder.add(builder.build());

        builder.add(new RowBean(DateUtil.createDate(2, 12, 2011), "", 7, RowBeanEnum.TWO));
        setBuilder.add(builder.build());

        builder.add((new RowBean(DateUtil.createDate(7, 12, 2011), "", 9, RowBeanEnum.ZERO)));
        builder.add((new RowBean(DateUtil.createDate(18, 12, 2011), "", 18, RowBeanEnum.ONE)));
        builder.add((new RowBean(DateUtil.createDate(23, 12, 2011), "", 4, RowBeanEnum.TWO)));
        setBuilder.add(builder.build());

        SETS = setBuilder.build();

    }

    protected static final Exporter<RowBean> EXPORTER = Exporter
            .<RowBean>builder()
            .setCellSeparator("~")
            .setEndOfLineSeparator("--\n")
            .setCharset(CHARSET)
            .addColumn("D~ATE", "date", Common.DAY)
            .addColumn("TITLEé", "title")
            .addColumn("NUMBER", "number", Common.INTEGER)
            .addColumn("title")
            .addColumn("rowBeanEnum", new ValueGetter<RowBean, String>() {

                @Override
                public String get(RowBean object) throws IOException {
                    return object.getRowBeanEnum().name();
                }
            })
            .build();

    @Test
    public void testExportToString() throws IOException {

        Iterator<String> resultIterator = RESULTS.iterator();
        for (Set<RowBean> set : SETS) {

            String csv = EXPORTER.toString(set);

            String csvWithoutHeader = EXPORTER.toStringWithoutHeader(set);

            assertExportResultEquals(resultIterator, csv, csvWithoutHeader);

        }

    }

    @Test
    public void testExportToWriter() throws IOException {

        Iterator<String> resultIterator = RESULTS.iterator();
        for (Set<RowBean> set : SETS) {

            String csv;
            try (StringWriter writer = new StringWriter()) {
                EXPORTER.toWriter(set, writer);
                csv = writer.toString();
            }

            String csvWithoutHeader;
            try (StringWriter writer = new StringWriter()) {
                EXPORTER.toWriterWithoutHeader(set, writer);
                csvWithoutHeader = writer.toString();
            }

            assertExportResultEquals(resultIterator, csv, csvWithoutHeader);

        }

    }

    @Test
    public void testExportToOutputStream() throws IOException {

        File parentFile = new File(FileUtils.getTempDirectory(), getClass().getName() + "-testExportToOutputStream");
        FileUtils.forceMkdir(parentFile);

        int index = 0;
        Iterator<String> resultIterator = RESULTS.iterator();
        for (Set<RowBean> set : SETS) {

            File exportFile = new File(parentFile, (index++) + ".csv");
            try (OutputStream outputStream = new FileOutputStream(exportFile)) {
                EXPORTER.toOutputStream(set, outputStream);
            }

            File exportFileWithoutHeader = new File(parentFile, (index) + "-withoutHeader.csv");
            try (OutputStream outputStream = new FileOutputStream(exportFileWithoutHeader)) {
                EXPORTER.toOutputStreamWithoutHeader(set, outputStream);
            }

            String csv = Files.toString(exportFile, CHARSET);
            String csvWithoutHeader = Files.toString(exportFileWithoutHeader, CHARSET);
            assertExportResultEquals(resultIterator, csv, csvWithoutHeader);

        }

    }

    @Test
    public void testExportToFile() throws IOException {

        File parentFile = new File(FileUtils.getTempDirectory(), getClass().getName() + "-testExportToFile");
        FileUtils.forceMkdir(parentFile);

        Iterator<String> resultIterator = RESULTS.iterator();

        int index = 0;
        for (Set<RowBean> set : SETS) {

            File exportFile = new File(parentFile, (index++) + ".csv");
            EXPORTER.toFile(set, exportFile);

            String csv = Files.toString(exportFile, CHARSET);

            File exportFileWithoutHeader = new File(parentFile, (index) + "-withoutHeader.csv");
            EXPORTER.toFileWithoutHeader(set, exportFileWithoutHeader);

            String csvWithoutHeader = Files.toString(exportFileWithoutHeader, CHARSET);

            assertExportResultEquals(resultIterator, csv, csvWithoutHeader);

        }

    }

    @Test
    public void testExportToReader() throws IOException {

        Iterator<String> resultIterator = RESULTS.iterator();
        for (Set<RowBean> set : SETS) {

            String csv;
            try (Reader reader = EXPORTER.toReader(set)) {
                csv = IOUtils.toString(reader);
            }

            String csvWithoutHeader;
            try (Reader reader = EXPORTER.toReaderWithoutHeader(set)) {
                csvWithoutHeader = IOUtils.toString(reader);
            }

            assertExportResultEquals(resultIterator, csv, csvWithoutHeader);

        }

    }

    @Test
    public void testExportToInputStream() throws IOException {

        Iterator<String> resultIterator = RESULTS.iterator();
        for (Set<RowBean> set : SETS) {

            String csv;
            try (InputStream inputStream = EXPORTER.toInputStream(set)) {
                csv = IOUtils.toString(inputStream);
            }
            String csvWithoutHeader;
            try (InputStream inputStream = EXPORTER.toInputStreamWithoutHeader(set)) {
                csvWithoutHeader = IOUtils.toString(inputStream);
            }
            assertExportResultEquals(resultIterator, csv, csvWithoutHeader);

        }

    }

    /**
     * See https://forge.nuiton.org/issues/3015
     *
     * @throws IOException
     */
    @Test
    public void testHeaderShouldBeEspace() throws IOException {

        Exporter<RowBean> exporter = Exporter.<RowBean>builder()
                .setCharset(CHARSET)
                .addColumn("a;", "date", Common.DAY)
                .addColumn("B\nb", "title")
                .build();

        Set<RowBean> set = new LinkedHashSet<>();
        set.add(new RowBean(DateUtil.createDate(1, 12, 2011), "One", 1, RowBeanEnum.ONE));
        set.add(new RowBean(DateUtil.createDate(1, 12, 2012), "Two", 1, RowBeanEnum.TWO));
        String csv = exporter.toString(set);
        String csvWithoutHeader = exporter.toStringWithoutHeader(set);

        Assert.assertEquals("\"a;\";\"B\n" +
                            "b\"\n" +
                            "01/12/2011;One\n" +
                            "01/12/2012;Two\n", csv);
        Assert.assertEquals("01/12/2011;One\n" +
                            "01/12/2012;Two\n", csvWithoutHeader);

    }

    protected void assertExportResultEquals(Iterator<String> resultIterator, String actual, String actualWithoutHeader) {

        String expected = resultIterator.next();
        String expectedWithoutHeader = StringUtils.substringAfter(expected, "\n");

        Assert.assertEquals(expected, actual);
        Assert.assertEquals(expectedWithoutHeader, actualWithoutHeader);

    }

}
