/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ExportTest {

    private static final Log log = LogFactory.getLog(ExportTest.class);

    public static final Charset CHARSET = Charset.forName("UTF-8");

    protected Set<RowBean> oneSizedSet = new HashSet<>();

    protected Set<RowBean> twoSizedSet = new HashSet<>();

    protected Set<RowBean> fiveSizedSet = new HashSet<>();

    protected List<Set<RowBean>> sets;

    protected ExportModel<RowBean> model = new RowBeanExportModel();

    @Before
    public void setUp() {

        oneSizedSet.add(new RowBean(DateUtil.createDate(1, 12, 2011), "Batman", 1, RowBeanEnum.ONE));

        twoSizedSet.addAll(oneSizedSet);
        twoSizedSet.add(new RowBean(DateUtil.createDate(2, 12, 2011), "", 7, RowBeanEnum.TWO));

        fiveSizedSet.addAll(twoSizedSet);
        fiveSizedSet.add(new RowBean(DateUtil.createDate(7, 12, 2011), "", 9, RowBeanEnum.ZERO));
        fiveSizedSet.add(new RowBean(DateUtil.createDate(18, 12, 2011), "", 18, RowBeanEnum.ONE));
        fiveSizedSet.add(new RowBean(DateUtil.createDate(23, 12, 2011), "", 4, RowBeanEnum.TWO));

        sets = Arrays.asList(new HashSet<RowBean>(), oneSizedSet, twoSizedSet, fiveSizedSet);
    }

    @Test
    public void testExportToString() throws Exception {

        for (Set<RowBean> set : sets) {

            String csv = Export.exportToString(model, set, CHARSET);

            if (log.isDebugEnabled()) {
                log.debug("exported csv:\n" + csv);
            }

            // 1 header line + one line per RowBean instance
            int expectedLineCount = 1 + set.size();
            // number of '\n' in csv
            int actualLineCount = csv.split("\n").length;
            Assert.assertEquals("exported CSV must have all lines",
                                expectedLineCount, actualLineCount);
        }

    }

    /**
     * See https://forge.nuiton.org/issues/3407
     */
    @Test
    public void testExportToOuputStream() throws Exception {

        File f = new File(FileUtils.getTempDirectory(), getClass().getName() + "-exportcsvtest.zip");
        
        FileUtils.forceMkdir(f.getParentFile());

        ZipOutputStream outputStream = new ZipOutputStream(new FileOutputStream(f));

        for (Set<RowBean> set : sets) {

            outputStream.putNextEntry(new ZipEntry("/export" + (set.size()) + ".csv"));
            Export.exportToOutputStream(model, set, outputStream, CHARSET);
        }

        outputStream.close();

        ZipFile zipFile = new ZipFile(f);

        for (Set<RowBean> set : sets) {

            String csv = IOUtils.toString(zipFile.getInputStream(new ZipEntry("/export" + (set.size()) + ".csv")));

            if (log.isDebugEnabled()) {
                log.debug("exported csv:\n" + csv);
            }

            // 1 header line + one line per RowBean instance
            int expectedLineCount = 1 + set.size();
            // number of '\n' in csv
            int actualLineCount = csv.split("\n").length;
            Assert.assertEquals("exported CSV must have all lines",
                                expectedLineCount, actualLineCount);
        }

    }

}
