/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import java.util.Date;
import java.util.List;

public class RowBean {

    private Date date;

    private String title;

    private Integer number;

    private RowBeanEnum rowBeanEnum;

    private List<Boolean> booleans;

    public RowBean() {
    }

    public RowBean(Date date, String title, Integer number, RowBeanEnum rowBeanEnum) {
        this.date = date;
        this.title = title;
        this.number = number;
        this.rowBeanEnum = rowBeanEnum;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public RowBeanEnum getRowBeanEnum() {
        return rowBeanEnum;
    }

    public void setRowBeanEnum(RowBeanEnum rowBeanEnum) {
        this.rowBeanEnum = rowBeanEnum;
    }

    public List<Boolean> getBooleans() {
        return booleans;
    }

    public void setBooleans(List<Boolean> booleans) {
        this.booleans = booleans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RowBean)) return false;

        RowBean row = (RowBean) o;

        if (!date.equals(row.date)) return false;
        if (!number.equals(row.number)) return false;
        if (!rowBeanEnum.equals(row.rowBeanEnum)) return false;
        return title.equals(row.title);

    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + number.hashCode();
        result = 31 * result + rowBeanEnum.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "RowBean{" +
               "date=" + date +
               ", title='" + title + '\'' +
               ", number=" + number +
               ", rowBeanEnum=" + rowBeanEnum +
               '}';
    }
}
