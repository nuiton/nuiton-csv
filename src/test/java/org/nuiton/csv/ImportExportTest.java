package org.nuiton.csv;

/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.csv.ext.AbstractImportExportModel;
import org.nuiton.util.DateUtil;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Simple test for import/export.
 *
 * @author Yannick Martel (martel@codelutin.com)
 * @since 3.0
 */
public class ImportExportTest {

    public static final Charset CHARSET = Charset.forName("UTF-8");

    protected ImportExportModel<RowBean> importExportModel;

    @Before
    public void setUp() {
        AbstractImportExportModel<RowBean> importExportModel = new AbstractImportExportModel<RowBean>(';') {
            @Override
            public RowBean newEmptyInstance() {
                return new RowBean();
            }
        };
        importExportModel.newColumnForImportExport("NUMBER", "number", Common.INTEGER);
        importExportModel.newColumnForImportExport("TITLE", "title", Common.STRING);
        importExportModel.newColumnForImportExport("DATE", "date", new Common.DateValue("yyyy-MM-dd"));
        importExportModel.newColumnForImportExport("ROWBEANENUM", "rowBeanEnum", Common.newEnumByNameParserFormatter(RowBeanEnum.class));
        importExportModel.newColumnForImportExport("BOOLS", "booleans", Common.newListParserFormatter(',', Common.BOOLEAN, null, false));

        this.importExportModel = importExportModel;
    }

    @Test
    public void testSimpleImport() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM;BOOLS\n" +
                         "2011-12-05;18;\"1ère ligne\";ZERO;Y,Y,N\n" +
                         "2011-12-06;19;\"2ème ligne\";ONE;N,N,N\n" +
                         "2011-12-07;21;\"3ème ligne\";TWO;Y,N,N";
        List<RowBean> rows = importContent(importExportModel, content);
        assertRowEquals(rows.get(0), DateUtil.createDate(5, 12, 2011), 18, "1ère ligne", RowBeanEnum.ZERO, Lists.newArrayList(Boolean.TRUE, Boolean.TRUE, Boolean.FALSE));
        assertRowEquals(rows.get(1), DateUtil.createDate(6, 12, 2011), 19, "2ème ligne", RowBeanEnum.ONE, Lists.newArrayList(Boolean.FALSE, Boolean.FALSE, Boolean.FALSE));
        assertRowEquals(rows.get(2), DateUtil.createDate(7, 12, 2011), 21, "3ème ligne", RowBeanEnum.TWO, Lists.newArrayList(Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));
    }

    @Test
    public void testExportToString() throws Exception {

        RowBean one = new RowBean(DateUtil.createDate(30, 3, 1939), "Batman", 7, RowBeanEnum.ZERO);
        one.setBooleans(Lists.newArrayList(Boolean.TRUE, Boolean.TRUE, Boolean.FALSE));

        RowBean two = new RowBean(DateUtil.createDate(5, 7, 1984), "Nightwing", 17, RowBeanEnum.ONE);
        two.setBooleans(Lists.newArrayList(Boolean.FALSE, Boolean.FALSE, Boolean.FALSE));

        Set<RowBean> rowBeans = Sets.newHashSet(one, two);

        String csv = Export.exportToString(importExportModel, rowBeans, CHARSET);

        // 1 header line + one line per RowBean instance
        int expectedLineCount = 1 + rowBeans.size();
        // number of '\n' in csv
        int actualLineCount = csv.split("\n").length;
        Assert.assertEquals("exported CSV must have all lines",
                            expectedLineCount, actualLineCount);

        Assert.assertTrue(csv.contains("Y,Y,N"));
        Assert.assertTrue(csv.contains("N,N,N"));

    }

    @Test
    public void testExportImportDoubleQuotesString() throws Exception {

        RowBean one = new RowBean(new Date(), "Un singe en été", 24, RowBeanEnum.TWO);
        one.setBooleans(Collections.singletonList(true));
        RowBean two = new RowBean(new Date(), "\"Amazing\" spiderman", 42, RowBeanEnum.ZERO);
        two.setBooleans(Collections.singletonList(false));

        List<RowBean> rowBeans = Arrays.asList(one, two);

        String csv = Export.exportToString(importExportModel, rowBeans, CHARSET);

        // 1 header line + one line per RowBean instance
        int expectedLineCount = 1 + rowBeans.size();
        // number of '\n' in csv
        int actualLineCount = csv.split("\n").length;
        Assert.assertEquals("exported CSV must have all lines",
                            expectedLineCount, actualLineCount);

        List<RowBean> imported = importContent(importExportModel, csv);

        // Compare titles only, we want to test double quotes
        Assert.assertEquals(one.getTitle(), imported.get(0).getTitle());
        Assert.assertEquals(two.getTitle(), imported.get(1).getTitle());

    }

    @Test
    public void testImportDoubleQuotesString() throws Exception {

        String csv = "DATE;TITLE;NUMBER;ROWBEANENUM;BOOLS\n" +
                "2018-05-02;Un singe en été;24;TWO;false\n" +
                "2018-05-02;\"Amazing\" spiderman;42;ZERO;true";

        List<RowBean> imported = importContent(importExportModel, csv);

        // Import doesn't fail, BUT as the CSV value is not correctly formatted, we accept that the double quotes
        // overtakes the full value, thus we accept that the title is "Amazing" only
        Assert.assertEquals("Amazing", imported.get(1).getTitle());

        // XXX AThimel 02/05/2018 This behavior might change in the future which means the next test will be valid
        // Assert.assertEquals(""Amazing" spiderman", imported.get(1).getTitle());

    }

    protected List<RowBean> importContent(ImportModel<RowBean> model,
                                          String content) throws IOException {
        try (
                Reader reader = new StringReader(content);
                Import2<RowBean> rowImport = Import2.newImport(model, reader)
        ){
            List<RowBean> result = new ArrayList<>();
            for (ImportRow<RowBean> row : rowImport) {
                result.add(row.getBean());
            }
            return result;
        }
    }

    private void assertRowEquals(RowBean row, Date date, Integer number, String title, RowBeanEnum rowBeanEnum, List<Boolean> booleans) {
        Assert.assertEquals(date, row.getDate());
        Assert.assertEquals(number, row.getNumber());
        Assert.assertEquals(title, row.getTitle());
        Assert.assertEquals(rowBeanEnum, row.getRowBeanEnum());
        Assert.assertEquals(booleans.size(), row.getBooleans().size());
        for (int i=0; i < booleans.size(); i++) {
            Assert.assertEquals(booleans.get(i), row.getBooleans().get(i));
        }
    }
}
