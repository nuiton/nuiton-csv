/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.csv.Common.BeanProperty;
import org.nuiton.csv.ext.AbstractImportModel;
import org.nuiton.util.DateUtil;

import com.google.common.collect.Lists;

/**
 * Created on 28/11/11
 *
 * @author Florian Desbois
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class ImportTest {

    @Test
    @Ignore
    public void testSimpleExportReImportWithSpecialChars() throws Exception {
        // create test data
        List<RowBean> rows = Lists.newArrayList();
        rows.add(new RowBean(new Date(), "Un singe en été", 24, RowBeanEnum.TWO));
        rows.add(new RowBean(new Date(), "\"Amazing\" spiderman", 42, RowBeanEnum.ZERO));
        
        // export as string
        String csv = Export.exportToString(new ExportModel<RowBean>() {
            @Override
            public char getSeparator() {
                return ';';
            }

            @Override
            public Iterable<ExportableColumn<RowBean, Object>> getColumnsForExport() {
                ModelBuilder<RowBean> modelBuilder = new ModelBuilder<>();
                modelBuilder.newColumnForExport("DATE", "date", Common.DAY);
                modelBuilder.newColumnForExport("TITLE", "title");
                modelBuilder.newColumnForExport("NUMBER", "number", Common.INTEGER);
                modelBuilder.newColumnForExport("ROWBEANENUM", "rowBeanEnum", Common.newEnumByNameParserFormatter(RowBeanEnum.class));
                return (Iterable) modelBuilder.getColumnsForExport();
            }
        }, rows, Charset.forName("UTF-8"));

        // import as string
        rows = importContent(new AbstractImportModel<RowBean>(';') {
            @Override
            public void pushCsvHeaderNames(List<String> headerNames) {
                super.pushCsvHeaderNames(headerNames);
                newMandatoryColumn("DATE", "date", Common.DAY);
                newMandatoryColumn("TITLE", "title");
                newMandatoryColumn("NUMBER", "number", Common.INTEGER);
                newMandatoryColumn("ROWBEANENUM", "rowBeanEnum", Common.newEnumByNameParserFormatter(RowBeanEnum.class));
            }
            @Override
            public RowBean newEmptyInstance() {
                return new RowBean();
            }
        }, csv);
        Assert.assertEquals(2, rows.size());
        Assert.assertEquals("Un singe en été", rows.get(0).getTitle());
        Assert.assertEquals("\"Amazing\" spiderman", rows.get(1).getTitle());
    }

    @Test
    public void testSimpleImportWithNoData() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM";

        List<RowBean> rows;
        rows = importContent(new SimpleImportModel(), content);
        Assert.assertEquals(0, rows.size());

        content = "DATE;NUMBER;TITLE;ROWBEANENUM\n";
        rows = importContent(new SimpleImportModel(), content);
        Assert.assertEquals(0, rows.size());
    }

    @Test
    public void testSimpleImportWithOneLine() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM\n2011-12-05;18;\"1ère ligne\";ZERO;";

        List<RowBean> rows = importContent(new SimpleImportModel(), content);
        Assert.assertEquals(1, rows.size());
        assertRowEquals(rows.get(0), DateUtil.createDate(5, 12, 2011), 18, "1ère ligne",RowBeanEnum.ZERO);
    }

    /**
     * Test with {@link SimpleImportModel} that directly implements necessary {@link ImportableColumn}.
     *
     * @throws Exception for errors
     */
    @Test
    public void testSimpleImport() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM\n" +
                "2011-12-05;18;\"1ère ligne\";ZERO\n" +
                "2011-12-06;19;\"2ème ligne\";ONE\n" +
                "2011-12-07;21;\"3ème ligne\";TWO";
        List<RowBean> rows = importContent(new SimpleImportModel(), content);
        assertRowEquals(rows.get(0), DateUtil.createDate(5, 12, 2011), 18, "1ère ligne",RowBeanEnum.ZERO);
        assertRowEquals(rows.get(1), DateUtil.createDate(6, 12, 2011), 19, "2ème ligne",RowBeanEnum.ONE);
        assertRowEquals(rows.get(2), DateUtil.createDate(7, 12, 2011), 21, "3ème ligne",RowBeanEnum.TWO);
    }

    /**
     * Test with {@link ColumnImportModel} that uses {@link Column} and {@link Common} tools.
     *
     * @throws Exception for errors
     */
    @Test
    public void testColumnImport() throws Exception {
        String content = "DATE;NUMBER;TITLE;ROWBEANENUM\n" +
                "2011-12-05;18;\"1ère ligne\";2\n" +
                "2011-12-06;19;\"2ème ligne\";1\n" +
                "2011-12-07;21;\"3ème ligne\";0";
        List<RowBean> rows = importContent(new ColumnImportModel(), content);
        assertRowEquals(rows.get(0), DateUtil.createDate(5, 12, 2011), 18, "1ère ligne",RowBeanEnum.TWO);
        assertRowEquals(rows.get(1), DateUtil.createDate(6, 12, 2011), 19, "2ème ligne",RowBeanEnum.ONE);
        assertRowEquals(rows.get(2), DateUtil.createDate(7, 12, 2011), 21, "3ème ligne",RowBeanEnum.ZERO);
    }

    protected List<RowBean> importContent(ImportModel<RowBean> model,
                                      String content) throws IOException {

        try (
                Reader reader = new StringReader(content);
                Import<RowBean> rowImport = Import.newImport(model, reader)
        ) {
            List<RowBean> result = new ArrayList<>();
            for (RowBean row : rowImport) {
                result.add(row);
            }
            return result;
        }
    }

    private void assertRowEquals(RowBean row, Date date, Integer number, String title,RowBeanEnum rowBeanEnum) {
        Assert.assertEquals(date, row.getDate());
        Assert.assertEquals(number, row.getNumber());
        Assert.assertEquals(title, row.getTitle());
        Assert.assertEquals(rowBeanEnum, row.getRowBeanEnum());
    }

    private static class ColumnImportModel implements ImportModel<RowBean> {

        @Override
        public char getSeparator() {
            return ';';
        }

        @Override
        public void pushCsvHeaderNames(List<String> headerNames) {
        }

        @Override
        public RowBean newEmptyInstance() {
            return new RowBean();
        }

        @SuppressWarnings({"unchecked"})
        @Override
        public Collection<ImportableColumn<RowBean, Object>> getColumnsForImport() {
            List<ImportableColumn<RowBean, Object>> result = new ArrayList<>();
            // Column types are not checked but safe because Object is necessary, and element type is always Row
            result.add(getTitleColumn());
            result.add(getNumberColumn());
            result.add(getDateColumn());
            result.add(getEnumColumn());
            return result;
        }

        private Column getTitleColumn() {
            return Column.newImportableColumn(
                    "TITLE",
                    Common.STRING,
                    withBeanSetter("title", String.class),
                    false,
                    true
            );
        }

        private Column getDateColumn() {
            return Column.newImportableColumn(
                    "DATE",
                    withDateParser(),
                    withBeanSetter("date", Date.class),
                    false,
                    true
            );
        }

        private Column getNumberColumn() {
            return Column.newImportableColumn(
                    "NUMBER",
                    Common.INTEGER,
                    withBeanSetter("number", Integer.class),
                    false,
                    true
            );
        }

        private Column getEnumColumn() {
            return Column.newImportableColumn(
                    "ROWBEANENUM",
                    Common.newEnumByOrdinalParserFormatter(RowBeanEnum.class),
                    withBeanSetter("rowBeanEnum", RowBeanEnum.class),
                    false,
                    true
            );
        }
        @SuppressWarnings({"UnusedParameters"})
        private <T> ValueSetter<RowBean, T> withBeanSetter(String propertyName, Class<T> propertyClass) {
            // propertyClass is not used but useful to check type
            return new BeanProperty<>(propertyName);
        }

        private ValueParser<Date> withDateParser() {
            return new ValueParser<Date>() {

                @Override
                public Date parse(String value)
                        throws ParseException {
                    return DateUtils.parseDate(value, "yyyy-MM-dd");
                }
            };
        }
    }

    private static class SimpleImportModel implements ImportModel<RowBean> {

        private Log log = LogFactory.getLog(SimpleImportModel.class);

        @Override
        public char getSeparator() {
            return ';';
        }

        @Override
        public void pushCsvHeaderNames(List<String> headerNames) {
            if (log.isDebugEnabled()) {
                log.debug("Headers are : " + headerNames);
            }
        }

        @Override
        public RowBean newEmptyInstance() {
            return new RowBean();
        }

        @Override
        public Collection<ImportableColumn<RowBean, Object>> getColumnsForImport() {
            List<ImportableColumn<RowBean, Object>> result = new ArrayList<>();
            result.add(getTitleImportable());
            result.add(getNumberImportable());
            result.add(getDateImportable());
            result.add(getEnumImportable());
            return result;
        }

        private ImportableColumn<RowBean, Object> getTitleImportable() {
            return new AbstractImportableColumn<RowBean, String>() {

                @Override
                public String getHeaderName() {
                    return "TITLE";
                }

                @Override
                public boolean isMandatory() {
                    return true;
                }

                @Override
                public boolean isIgnored() {
                    return false;
                }

                @Override
                public String parseValue(String value) {
                    return value;
                }

                @Override
                public void update(RowBean object,
                                   String value)
                        throws Exception {
                    object.setTitle(value);
                }
            };
        }

        private ImportableColumn<RowBean, Object> getDateImportable() {
            return new AbstractImportableColumn<RowBean, Date>() {

                @Override
                public String getHeaderName() {
                    return "DATE";
                }

                @Override
                public boolean isMandatory() {
                    return true;
                }

                @Override
                public boolean isIgnored() {
                    return false;
                }

                @Override
                public Date parseValue(String value)
                        throws ParseException {
                    return DateUtils.parseDate(value, "yyyy-MM-dd");
                }

                @Override
                public void update(RowBean object,
                                   Date value)
                        throws Exception {
                    object.setDate(value);
                }
            };
        }

        private ImportableColumn<RowBean, Object> getNumberImportable() {
            return new AbstractImportableColumn<RowBean, Integer>() {

                @Override
                public String getHeaderName() {
                    return "NUMBER";
                }

                @Override
                public boolean isMandatory() {
                    return true;
                }

                @Override
                public boolean isIgnored() {
                    return false;
                }

                @Override
                public Integer parseValue(String value) {
                    return Integer.parseInt(value);
                }

                @Override
                public void update(RowBean object,
                                   Integer value)
                        throws Exception {
                    object.setNumber(value);
                }
            };
        }

        private ImportableColumn<RowBean, Object> getEnumImportable() {
            return new AbstractImportableColumn<RowBean, RowBeanEnum>() {

                @Override
                public String getHeaderName() {
                    return "ROWBEANENUM";
                }

                @Override
                public boolean isMandatory() {
                    return true;
                }

                @Override
                public boolean isIgnored() {
                    return false;
                }

                @Override
                public RowBeanEnum parseValue(String value) {
                    return RowBeanEnum.valueOf(value);
                }

                @Override
                public void update(RowBean object,
                                   RowBeanEnum value)
                        throws Exception {
                    object.setRowBeanEnum(value);
                }
            };
        }
    }

    private static abstract class AbstractImportableColumn<E, F> implements ImportableColumn<E, Object> {

        @Override
        public void setValue(E object,
                             Object value)
                throws Exception {
            update(object, (F) value);
        }

        protected abstract void update(E object, F value)
                throws Exception;
    }

}
