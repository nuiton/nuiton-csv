/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class MyModelBuilderTest {

    @Test
    public void testNewEmptyModel() throws Exception {

        String content = "DATE;NUMBER;TITLE;ROWBEANENUM\n" +
                         "25/12/2011;18;\"1ère ligne\";ZERO\n" +
                         "26/12/2011;19;\"2ème ligne\";ONE\n" +
                         "27/12/2011;21;\"3ème ligne\";TWO";

        CsvModel.Factory<RowBean> emptyRowBeanFactory = new CsvModel.Factory<RowBean>() {
            @Override
            public RowBean newEmpty() {
                return new RowBean();
            }
        };

        CsvModel<RowBean> model = new MyModelBuilder<RowBean>().newEmptyModel(';')
                .addColumn("DATE")
                        .mandatoryAtImport("date", Common.DAY)
                        .writeAtExport("date", Common.DAY)
                        .add()
                .addColumn("TITLE")
                        .mandatoryAtImport("title")
                        .ignoredAtExport()
                        .add()
                .addColumn("NUMBER")
                        .mandatoryAtImport("number", Common.INTEGER)
                        .ignoredAtExport()
                        .add()
                .addColumn("ROWBEANENUM")
                        .mandatoryAtImport("rowBeanEnum", Common.newEnumByNameParserFormatter(RowBeanEnum.class))
                        .ignoredAtExport()
                        .add()
                .buildModelForImport(emptyRowBeanFactory);

        Import<RowBean> rowBeans = Import.newImport(model, IOUtils.toInputStream(content, "UTF-8"));

        List<RowBean> list = new LinkedList<>();
        for (RowBean rowBean : rowBeans) {
            list.add(rowBean);
        }

        Assert.assertEquals(3, list.size());
        Assert.assertEquals(18, (int)list.get(0).getNumber());
        Assert.assertEquals("2ème ligne", list.get(1).getTitle());
        Assert.assertEquals(RowBeanEnum.TWO, list.get(2).getRowBeanEnum());
    }

}
