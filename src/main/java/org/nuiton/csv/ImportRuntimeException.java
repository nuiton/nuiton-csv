/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class ImportRuntimeException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public ImportRuntimeException() {
    }

    public ImportRuntimeException(String message) {
        super(message);
    }

    public ImportRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ImportRuntimeException(Throwable cause) {
        super(cause);
    }
}
