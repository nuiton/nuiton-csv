/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import java.util.List;

/**
 * @param <E>
 */
public class CsvModel<E> implements ImportExportModel<E> {

    public interface Factory<E> {
        E newEmpty();
    }

    protected char separator;

    protected Iterable<ImportableColumn<E, Object>> columnsForImport;

    protected Iterable<ExportableColumn<E, Object>> columnsForExport;

    protected Factory<E> factory;

    public CsvModel(char separator, Iterable<ImportableColumn<E, Object>> columnsForImport, Iterable<ExportableColumn<E, Object>> columnsForExport, Factory<E> factory) {
        this.separator = separator;
        this.columnsForImport = columnsForImport;
        this.columnsForExport = columnsForExport;
        this.factory = factory;
    }

    @Override
    public char getSeparator() {
        return separator;
    }

    @Override
    public void pushCsvHeaderNames(List<String> headerNames) {
        // 
    }

    @Override
    public E newEmptyInstance() {
        return factory.newEmpty();
    }

    @Override
    public Iterable<ImportableColumn<E, Object>> getColumnsForImport() {
        return columnsForImport;
    }

    @Override
    public Iterable<ExportableColumn<E, Object>> getColumnsForExport() {
        return columnsForExport;
    }
}
