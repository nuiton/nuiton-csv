/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit, Brendan Le Ny
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import java.text.ParseException;

/**
 * TODO
 *
 * @author Brendan Le Ny - leny@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class Column<E, T> implements ImportableExportableColumn<E, T> {

    public static <E, T> Column<E, T> newExportableColumn(String headerName,
                                                          ValueGetter<E, T> valueGetter,
                                                          ValueFormatter<T> valueFormatter,
                                                          boolean ignored) {
        return new Column<>(headerName,
                valueGetter,
                valueFormatter,
                null,
                null,
                ignored,
                !ignored
        );
    }

    public static <E, T> Column<E, T> newImportableColumn(String headerName,
                                                          ValueParser<T> valueParser,
                                                          ValueSetter<E, T> valueSetter,
                                                          boolean ignored,
                                                          boolean mandatory) {
        return new Column<>(headerName,
                null,
                null,
                valueParser,
                valueSetter,
                ignored,
                mandatory
        );
    }

    public static <E, T> Column<E, T> newImportableExportableColumn(String headerName,
                                                                    ValueGetter<E, T> valueGetter,
                                                                    ValueFormatter<T> valueFormatter,
                                                                    ValueParser<T> valueParser,
                                                                    ValueSetter<E, T> valueSetter,
                                                                    boolean ignored) {
        return new Column<>(headerName,
                valueGetter,
                valueFormatter,
                valueParser,
                valueSetter,
                ignored,
                !ignored
        );
    }

    public static <E, T> Column<E, T> newImportableExportableColumn(String headerName,
                                                                    ValueGetterSetter<E, T> valueGetterSetter,
                                                                    ValueParserFormatter<T> valueParserFormatter,
                                                                    boolean ignored) {
        return newImportableExportableColumn(headerName,
                                             valueGetterSetter,
                                             valueParserFormatter,
                                             valueParserFormatter,
                                             valueGetterSetter,
                                             ignored
        );
    }

    protected String headerName;

    protected boolean mandatory = true;

    protected boolean ignored;

    protected ValueParser<T> valueParser;

    protected ValueFormatter<T> valueFormatter;

    protected ValueGetter<E, T> valueGetter;

    protected ValueSetter<E, T> valueSetter;

    protected Column(String headerName,
                     ValueGetter<E, T> valueGetter,
                     ValueFormatter<T> valueFormatter,
                     ValueParser<T> valueParser,
                     ValueSetter<E, T> valueSetter,
                     boolean ignored,
                     boolean mandatory) {
        this.headerName = headerName;
        this.valueGetter = valueGetter;
        this.valueFormatter = valueFormatter;
        this.valueSetter = valueSetter;
        this.valueParser = valueParser;
        this.ignored = ignored;
        this.mandatory = mandatory;
    }

    @Override
    public String getHeaderName() {
        return headerName;
    }

    @Override
    public boolean isMandatory() {
        return mandatory;
    }

    @Override
    public boolean isIgnored() {
        return ignored;
    }

    @Override
    public String formatValue(T value) {
        if (valueFormatter == null) {
            throw new UnsupportedOperationException("no formatter provided for " + this);
        } else {
            return valueFormatter.format(value);
        }
    }

    @Override
    public T parseValue(String value) throws ParseException {
        if (valueParser == null) {
            throw new UnsupportedOperationException("no parser provided for " + this);
        } else {
            return valueParser.parse(value);
        }
    }

    @Override
    public T getValue(E object) throws Exception {
        if (valueGetter == null) {
            throw new UnsupportedOperationException("no getter provided for " + this);
        } else {
            return valueGetter.get(object);
        }
    }

    @Override
    public void setValue(E object, T value) throws Exception {
        if (!isIgnored()) {
            if (valueSetter == null) {
                throw new UnsupportedOperationException("no setter provided for " + this);
            } else {
                valueSetter.set(object, value);
            }
        }
    }

    @Override
    public String toString() {
        return "{" +
               "headerName='" + headerName + '\'' +
               ", mandatory=" + mandatory +
               ", ignored=" + ignored +
               '}';
    }
}
