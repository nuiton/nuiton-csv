package org.nuiton.csv;

/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.csvreader.CsvReader;

/**
 * To configure an import using the {@link Import2}.
 * <p>
 * If you do not give this object to the {@link Import2}, then it will
 * instanciate a new one using all default values.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.3
 */
public class ImportConf {

    /**
     * Flag to turn or not the safetySwitch (see {@link CsvReader#getSafetySwitch()}).
     * <p>
     * By default, not used.
     */
    protected boolean safetySwitch = false;

    /**
     * Flag to use a strict mode (says import will failed at the first error),
     * if setted to {@code false}, then errors for each rows will be stored in
     * the current row and import will continue to the end.
     * <p>
     * By default, used (strict mode).
     */
    protected boolean strictMode = true;

    /**
     * Flag to ignore header found in a import file and not declared in the
     * import model.
     * <p>
     * By default, not used (strict mode).
     */
    protected boolean ignoreUnknownHeader = false;

    /**
     * Flag to clone the import row while iterating on it.
     *
     * If not set then a shared instance of ImportRow is used, this means that you can't copy the iterable into a another one.
     * @since 3.0
     */
    protected boolean cloneImportRow = false;

    /**
     * Flag to ask for raw record in result.
     * <p>
     * By default, raw record are not included.
     *
     * @since 3.0
     */
    protected boolean captureRawRecord = false;

    public boolean isSafetySwitch() {
        return safetySwitch;
    }

    public void setSafetySwitch(boolean safetySwitch) {
        this.safetySwitch = safetySwitch;
    }

    public boolean isStrictMode() {
        return strictMode;
    }

    public void setStrictMode(boolean strictMode) {
        this.strictMode = strictMode;
    }

    public boolean isIgnoreUnknownHeader() {
        return ignoreUnknownHeader;
    }

    public void setIgnoreUnknownHeader(boolean ignoreUnknownHeader) {
        this.ignoreUnknownHeader = ignoreUnknownHeader;
    }

    public boolean isCloneImportRow() {
        return cloneImportRow;
    }

    public void setCloneImportRow(boolean cloneImportRow) {
        this.cloneImportRow = cloneImportRow;
    }

    public boolean isCaptureRawRecord() {
        return captureRawRecord;
    }

    public void setCaptureRawRecord(boolean captureRawRecord) {
        this.captureRawRecord = captureRawRecord;
    }

}
