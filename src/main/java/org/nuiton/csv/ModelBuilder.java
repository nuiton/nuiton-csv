/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit, Brendan Le Ny
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import java.util.Collection;
import java.util.LinkedList;

/**
 *
 *
 * @author Brendan Le Ny - leny@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class ModelBuilder<E> {

    protected Collection<ImportableColumn<E, ?>> columnsForImport =
            new LinkedList<>();

    protected Collection<ExportableColumn<E, ?>> columnsForExport =
            new LinkedList<>();

    public <T> ImportableColumn<E, T> newIgnoredColumn(String headerName) {
        Column<E, T> newColumn = Column.newImportableColumn(headerName, null, null, true, false);
        columnsForImport.add(newColumn);
        return newColumn;
    }

    public ImportableColumn<E, String> newMandatoryColumn(String headerName, String propertyName) {
        return newMandatoryColumn(headerName, propertyName, Common.STRING);
    }

    public <T> ImportableColumn<E, T> newMandatoryColumn(String headerName, String propertyName, ValueParser<T> valueParser) {
        return newMandatoryColumn(headerName, valueParser, new Common.BeanProperty<E, T>(propertyName));
    }
    
    public ImportableColumn<E, String> newMandatoryColumn(String headerName, ValueSetter<E, String> valueSetter) {
        return newMandatoryColumn(headerName, Common.STRING, valueSetter);
    }

    public <T> ImportableColumn<E, T> newMandatoryColumn(String headerName, ValueParser<T> valueParser, ValueSetter<E, T> valueSetter) {
        Column<E, T> newColumn = Column.newImportableColumn(headerName, valueParser, valueSetter, false, true);
        columnsForImport.add(newColumn);
        return newColumn;
    }

    public ImportableColumn<E, String> newOptionalColumn(String headerName, String propertyName) {
        return newOptionalColumn(headerName, propertyName, Common.STRING);
    }

    public <T> ImportableColumn<E, T> newOptionalColumn(String headerName, String propertyName, ValueParser<T> valueParser) {
        return newOptionalColumn(headerName, valueParser, new Common.BeanProperty<E, T>(propertyName));
    }

    public ImportableColumn<E, String> newOptionalColumn(String headerName, ValueSetter<E, String> valueSetter) {
        return newOptionalColumn(headerName, Common.STRING, valueSetter);
    }

    public <T> ImportableColumn<E, T> newOptionalColumn(String headerName, ValueParser<T> valueParser, ValueSetter<E, T> valueSetter) {
        Column<E, T> newColumn = Column.newImportableColumn(headerName, valueParser, valueSetter, false, false);
        columnsForImport.add(newColumn);
        return newColumn;
    }

    public ExportableColumn<E, String> newColumnForExport(String headerName, String propertyName) {
        return newColumnForExport(headerName, propertyName, Common.STRING);
    }

    public ExportableColumn<E, String> newColumnForExport(String headerName, ValueGetter<E, String> valueGetter) {
        return newColumnForExport(headerName, valueGetter, Common.STRING);
    }

    public <T> ExportableColumn<E, T> newColumnForExport(String headerName, String propertyName, ValueFormatter<T> valueFormatter) {
        return newColumnForExport(headerName, new Common.BeanProperty<E, T>(propertyName), valueFormatter);
    }

    public <T> ExportableColumn<E, T> newColumnForExport(String headerName, ValueGetter<E, T> valueGetter, ValueFormatter<T> valueFormatter) {
        ExportableColumn<E, T> newColumn = Column.newExportableColumn(headerName, valueGetter, valueFormatter, false);
        columnsForExport.add(newColumn);
        return newColumn;
    }

    public ImportableExportableColumn<E, String> newColumnForImportExport(String headerName, String propertyName) {
        return newColumnForImportExport(headerName, propertyName, Common.STRING);
    }

    public ImportableExportableColumn<E, String> newColumnForImportExport(String headerName, ValueGetterSetter<E, String> valueGetterSetter) {
        return newColumnForImportExport(headerName, valueGetterSetter, Common.STRING);
    }

    public <T> ImportableExportableColumn<E, T> newColumnForImportExport(String headerName, String propertyName, ValueParserFormatter<T> valueParserFormatter) {
        return newColumnForImportExport(headerName, new Common.BeanProperty<E, T>(propertyName), valueParserFormatter);
    }

    public <T> ImportableExportableColumn<E, T> newColumnForImportExport(String headerName, ValueGetterSetter<E, T> valueGetterSetter, ValueParserFormatter<T> valueParserFormatter) {
        ImportableExportableColumn<E, T> newColumn = Column.newImportableExportableColumn(headerName, valueGetterSetter, valueParserFormatter, false);
        columnsForImport.add(newColumn);
        columnsForExport.add(newColumn);
        return newColumn;
    }

    public Collection<ImportableColumn<E, ?>> getColumnsForImport() {
        return columnsForImport;
    }

    public Collection<ExportableColumn<E, ?>> getColumnsForExport() {
        return columnsForExport;
    }
}
