/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit, Brendan Le Ny
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Object to realize a export from a {@link ExportModel} and some datas.
 * <h2>Usefull static methods</h2>
 * There is some usefull methods here to do operation in one line :
 * <h3>To export to a file</h3>
 * <pre>
 *     Export.exportToFile(model, data, file);
 * </pre>
 * <h3>To export to a writer</h3>
 * <pre>
 *     Export.exportToWriter(model, data, writer);
 * </pre>
 * <h3>To export as a string</h3>
 * <pre>
 *     String exportcontent = Export.exportToString(model, data);
 * </pre>
 * <h3>To obtain a new instance of an exporter</h3>
 * <pre>
 *     Export&lt;E&gt; exporter = Export.newExport(model, data);
 * </pre>
 *
 * @author Brendan Le Ny - leny@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
@SuppressWarnings("ALL")
public class Export<E> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Export.class);

    /** Export model. */
    protected final ExportModel<E> model;

    /** Data to export. */
    protected Iterable<E> data;

    /**
     * Cell separator.
     *
     * @see ExportModel#getSeparator()
     */
    protected final String separator;

    public static <E> Export<E> newExport(ExportModel<E> model,
                                          Iterable<E> data) {
        return new Export<E>(model, data);
    }

    public static <E> void exportToOutputStream(ExportModel<E> model,
                                                Iterable<E> data,
                                                OutputStream outputStream) throws Exception {
        Export<E> exporter = newExport(model, data);
        exporter.write(outputStream, Charset.defaultCharset());
    }

    public static <E> void exportToOutputStream(ExportModel<E> model,
                                                Iterable<E> data,
                                                OutputStream outputStream,
                                                boolean writeHeader) throws Exception {
        Export<E> exporter = newExport(model, data);
        exporter.write(outputStream, Charset.defaultCharset(), writeHeader);
    }

    public static <E> void exportToOutputStream(ExportModel<E> model,
                                                Iterable<E> data,
                                                OutputStream outputStream,
                                                Charset charset) throws Exception {
        Export<E> exporter = newExport(model, data);
        exporter.write(outputStream, charset);
    }

    public static <E> void exportToOutputStream(ExportModel<E> model,
                                                Iterable<E> data,
                                                OutputStream outputStream,
                                                boolean writeHeader,
                                                Charset charset) throws Exception {
        Export<E> exporter = newExport(model, data);
        exporter.write(outputStream, charset, writeHeader);
    }

    public static <E> void exportToWriter(ExportModel<E> model,
                                          Iterable<E> data,
                                          Writer writer) throws Exception {
        Export<E> exporter = newExport(model, data);
        exporter.write(writer);
    }

    public static <E> void exportToWriter(ExportModel<E> model,
                                          Iterable<E> data,
                                          Writer writer,
                                          boolean writeHeader) throws Exception {
        Export<E> exporter = newExport(model, data);
        exporter.write(writer, writeHeader);
    }

    public static <E> void exportToFile(ExportModel<E> model,
                                        Iterable<E> data,
                                        File file) throws Exception {
        Export<E> exporter = newExport(model, data);
        exporter.write(file);
    }

    public static <E> void exportToFile(ExportModel<E> model,
                                        Iterable<E> data,
                                        File file,
                                        Charset charset) throws Exception {
        Export<E> exporter = newExport(model, data);
        exporter.write(file, charset);
    }

    public static <E> void exportToFile(ExportModel<E> model,
                                        Iterable<E> data,
                                        File file,
                                        Charset charset,
                                        boolean writeHeader) throws Exception {
        Export<E> exporter = newExport(model, data);
        exporter.write(file, charset, writeHeader);
    }

    public static <E> String exportToString(ExportModel<E> model,
                                            Iterable<E> data) throws Exception {
        Export<E> exporter = newExport(model, data);
        return exporter.toString(Charset.defaultCharset());
    }

    public static <E> String exportToString(ExportModel<E> model,
                                            Iterable<E> data,
                                            Charset charset) throws Exception {
        Export<E> exporter = newExport(model, data);
        return exporter.toString(charset);
    }

    public static <E> String exportToString(ExportModel<E> model,
                                            Iterable<E> data,
                                            Charset charset,
                                            boolean writeHeader) throws Exception {
        Export<E> exporter = newExport(model, data);
        return exporter.toString(charset, writeHeader);
    }

    protected Export(ExportModel<E> model, Iterable<E> data) {
        this.model = model;
        this.data = data;
        separator = String.valueOf(model.getSeparator());
    }

    public void write(Writer writer) throws Exception {
        write(writer, true);
    }

    public void write(Writer writer, boolean writeHeader) throws Exception {

        // write csv header
        if (writeHeader) {
            writeHeader(writer);
        }

        // obtain export columns
        Iterable<ExportableColumn<E, Object>> columns =
                model.getColumnsForExport();

        for (E row : data) {

            // write the row for this data
            writeRow(writer, columns, row);
        }
    }

    protected void writeHeader(Writer writer) throws IOException {

        List<String> headerNames = new LinkedList<String>();
        for (ExportableColumn<E, ?> column : model.getColumnsForExport()) {
            headerNames.add(column.getHeaderName());
        }
        String headersLine = StringUtil.join(headerNames, separator, true);
        writer.write(headersLine);
        writer.write('\n');
        if (log.isDebugEnabled()) {
            log.debug("headers for export are '" + headersLine + "'");
            if (data instanceof Collection) {
                log.debug("will export " + ((Collection<E>) data).size() + " lines");
            }
        }
    }

    protected void writeRow(Writer writer,
                            Iterable<ExportableColumn<E, Object>> columns,
                            E row) throws Exception {

        List<String> formattedCells = new LinkedList<String>();
        for (ExportableColumn<E, Object> column : columns) {
            Object cell = column.getValue(row);
            String formattedCell = column.formatValue(cell);
            if (formattedCell == null) {
                throw new NullPointerException(
                        "column for header " + column.getHeaderName() +
                        " returned a null value." + column.toString());
            }
            formattedCell = StringUtil.escapeCsvValue(formattedCell, separator);
            formattedCells.add(formattedCell);
        }
        String formattedRow = StringUtil.join(formattedCells, separator, true);
        writer.write(formattedRow);
        writer.write('\n');
    }

    public void write(OutputStream outputStream, Charset charset) throws Exception {
        write(outputStream, charset, true);
    }

    public void write(OutputStream outputStream, Charset charset, boolean writeHeader) throws Exception {
        Writer writer = new OutputStreamWriter(outputStream, charset);
        write(writer, writeHeader);
        writer.flush();
    }

    public void write(OutputStream outputStream) throws Exception {
        write(outputStream, Charset.defaultCharset());
    }

    public void write(OutputStream outputStream, boolean writeHeader) throws Exception {
        write(outputStream, Charset.defaultCharset(), writeHeader);
    }

    public void write(File file, Charset charset) throws Exception {
        write(file, charset, true);
    }

    public void write(File file, Charset charset, boolean writeHeader) throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        try {
            write(fileOutputStream, charset, writeHeader);
        } finally {
            fileOutputStream.close();
        }
    }

    public void write(File file) throws Exception {
        write(file, true);
    }

    public void write(File file, boolean writeHeader) throws Exception {
        write(file, Charset.defaultCharset(), writeHeader);
    }

    public String toString(Charset charset) throws Exception {
        return toString(charset, true);
    }

    public String toString(Charset charset, boolean writeHeader) throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        write(outputStream, charset, writeHeader);
        byte[] bytes = outputStream.toByteArray();
        String result = new String(bytes, charset);
        return result;
    }

}
