/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit, Brendan Le Ny
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 * TODO
 *
 * @author Brendan Le Ny - leny@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class ValidationResult implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SUCCESS = "success";

    public static final String PROPERTY_MESSAGE = "message";

    protected boolean success;

    protected String message;

    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public boolean getSuccess() {
        return success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        boolean oldValue = getSuccess();
        this.success = success;
        firePropertyChange(PROPERTY_SUCCESS, oldValue, success);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        String oldValue = getMessage();
        this.message = message;
        firePropertyChange(PROPERTY_MESSAGE, oldValue, message);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    @Override
    public String toString() {
        String toString = ToStringBuilder.reflectionToString(this);
        return toString;
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }


    protected void firePropertyChange(String propertyName, Object newValue) {
        firePropertyChange(propertyName, null, newValue);
    }


} //ValidationResult
