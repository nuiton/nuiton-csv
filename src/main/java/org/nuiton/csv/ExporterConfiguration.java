package org.nuiton.csv;

/*-
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2013 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;

import java.nio.charset.Charset;

/**
 * Configuration of an {@link Exporter}.
 *
 * Created on 09/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ExporterConfiguration<O> {

    /**
     * Columns export model.
     */
    protected ImmutableSet<ExportableColumn<O, ?>> columns;

    /**
     * Cell separator.
     */
    protected String cellSeparator;

    /**
     * Eof of line separator.
     */
    protected String endOfLineSeparator;

    /**
     * Charset used to perform the export.
     */
    protected Charset charset;

    public String getCellSeparator() {
        return cellSeparator;
    }

    public String getEndOfLineSeparator() {
        return endOfLineSeparator;
    }

    public Charset getCharset() {
        return charset;
    }

    public ImmutableSet<ExportableColumn<O, ?>> getColumns() {
        return columns;
    }

    public void setCellSeparator(String cellSeparator) {
        this.cellSeparator = cellSeparator;
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    public void setColumns(ImmutableSet<ExportableColumn<O, ?>> columns) {
        this.columns = columns;
    }

    public void setEndOfLineSeparator(String endOfLineSeparator) {
        this.endOfLineSeparator = endOfLineSeparator;
    }
}
