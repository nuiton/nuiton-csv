package org.nuiton.csv;

/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Abstract import esxception which contains the {@link ImportRow} object.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.3
 */
public abstract class AbstractImportErrorInfo<E> {

    protected final ImportableColumn<E, Object> field;

    protected final long lineNumber;

    protected final E bean;

    protected final Throwable cause;

    protected AbstractImportErrorInfo(ImportRow<E> row,
                                      ImportableColumn<E, Object> field,
                                      Throwable cause) {
        this.cause = cause;
        this.field = field;
        lineNumber = row.getLineNumber();
        bean = row.getBean();
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public E getBean() {
        return bean;
    }

    public ImportableColumn<E, Object> getField() {
        return field;
    }

    public Throwable getCause() {
        return cause;
    }
}
