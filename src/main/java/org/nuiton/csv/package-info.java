/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * This package contains a framework to import and export data from a
 * csv file using a model which permits us to validate what to do.
 *
 * <h2>Exporter API (since 3.0)</h2>
 * <h3>Steps to export</h3>
 * <ul>
 * <li>create an exporter (and configure it)</li>
 * <li>perform export using the exporter</li>
 * </ul>
 *
 * <h3>Example from a legacy model (with default configuration values)</h3>
 * <pre>
 *     ExportModel&lt;O&gt; model = ...;
 *     Exporter&lt;O&gt; exporter = Exporter.of(model);
 *     Iterable&lt;O&gt; data = ...;
 *     String csv = exporter.writeToString(data);
 * </pre>
 *
 * <h3>Example from scratch</h3>
 * <pre>
 *     ExportModel&lt;O&gt; model = ...;
 *     Exporter&lt;O&gt; exporter = Exporter
 *                                  .&lt;O&gt;builder()
 *                                  .addColum("propertyName")
 *                                  .addColum("headerName", "propertyName")
 *                                  .setCellSeparator(",")
 *                                  .build();
 *     Iterable&lt;O&gt; data = ...;
 *     String csv = exporter.writeToString(data);
 * </pre>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
package org.nuiton.csv;
