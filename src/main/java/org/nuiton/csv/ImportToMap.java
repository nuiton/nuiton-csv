/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import java.io.InputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * A extended {@link Import} to read csv lines into a single map.
 * <p>
 * <strong>Warning:</strong> The map used to push values for a csv line is the
 * same for all lines, it means you have to copy to your own object.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class ImportToMap extends Import<Map<String, Object>> {

    public static ImportToMap newImportToMap(ImportModel<Map<String, Object>> model,
                                             InputStream inputStream) {
        return new ImportToMap(model, inputStream);
    }

    public static ImportToMap newImportToMap(ImportModel<Map<String, Object>> model,
                                             Reader reader) {
        return new ImportToMap(model, reader);
    }

    public static ImportToMap newImportToMap(ImportModel<Map<String, Object>> model,
                                             InputStream inputStream, boolean safetySwitch) {
        return new ImportToMap(model, inputStream, safetySwitch);
    }

    public static ImportToMap newImportToMap(ImportModel<Map<String, Object>> model,
                                             Reader reader, boolean safetySwitch) {
        return new ImportToMap(model, reader, safetySwitch);
    }

    @Override
    public Iterator<Map<String, Object>> iterator() {

        // obtain headers from csv input and validate the model
        prepareAndValidate();

        return new Iterator<Map<String, Object>>() {

            // read first line since first line is header
            boolean hasNext = readRow();

            // get once for all columns to import
            List<ImportableColumn<Map<String, Object>, Object>> columns =
                    getNonIgnoredHeaders();

            // to stock the current line number
            int lineNumber;

            // the map where to object of a row
            final Map<String, Object> element = new HashMap<>();

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public Map<String, Object> next()
                    throws NoSuchElementException, ImportRuntimeException {

                if (!hasNext) {
                    throw new NoSuchElementException();
                }

                lineNumber += 1;

                // clean all values from the element
                element.clear();

                for (ImportableColumn<Map<String, Object>, Object> field :
                        columns) {

                    // read value from csv cell
                    String value = readValue(field, lineNumber);

                    // contravariance ftw
                    Object parsedValue = parseValue(field, lineNumber, value);

                    // set value to element
                    setValue(field, lineNumber, element, parsedValue);
                }

                // check if there is a next row to read
                hasNext = readRow();

                return element;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    protected ImportToMap(ImportModel<Map<String, Object>> mapImportModel,
                          InputStream inputStream) {
        this(mapImportModel, inputStream, true);
    }

    protected ImportToMap(ImportModel<Map<String, Object>> mapImportModel,
                          Reader reader) {
        this(mapImportModel, reader, true);
    }

    protected ImportToMap(ImportModel<Map<String, Object>> mapImportModel,
                          InputStream inputStream, boolean safetySwitch) {
        super(mapImportModel, inputStream);
        this.reader.setSafetySwitch(safetySwitch);
    }

    protected ImportToMap(ImportModel<Map<String, Object>> mapImportModel,
                          Reader reader, boolean safetySwitch) {
        super(mapImportModel, reader);
        this.reader.setSafetySwitch(safetySwitch);
    }
}
