package org.nuiton.csv;

/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collections;
import java.util.Set;

/**
 * Object to box a row to import.
 * <p>
 * It contains all the context for the current row to import.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6.3
 */
public class ImportRow<E> {

    /** Current line number. */
    protected long lineNumber;

    /** Current bean loaded by the import tool for the current row. */
    protected E bean;

    /** Errors found while loading the row into the bean. */
    protected Set<AbstractImportErrorInfo<E>> errors;

    protected boolean next;

    /**
     * The source data. May be empty if {@link ImportConf#captureRawRecord} is false.
     * When project will be Java8 based, use Java8 Optional
     */
    protected Optional<String> rawRecord = Optional.absent();

    public ImportRow(ImportRow<E> row) {
        this.lineNumber = row.getLineNumber();
        this.bean = row.getBean();
        this.errors = Sets.newHashSet(row.getErrors());
        this.setNext(row.hasNext());
        this.rawRecord = row.getRawRecord();
    }

    public ImportRow() {
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public E getBean() {
        return bean;
    }

    public Set<AbstractImportErrorInfo<E>> getErrors() {
        return errors == null ? Collections.<AbstractImportErrorInfo<E>>emptySet() : errors;
    }

    public boolean isValid() {
        return CollectionUtils.isEmpty(errors);
    }

    public boolean hasNext() {
        return next;
    }

    public void setNext(boolean next) {
        this.next = next;
    }

    public void setLineNumber(long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public void addError(AbstractImportErrorInfo<E> error) {
        if (errors == null) {
            errors = Sets.newHashSet();
        }
        errors.add(error);
    }

    public void prepareNextRow(E bean) {
        this.bean = bean;
        lineNumber++;
        errors = null;
        rawRecord = Optional.absent();
    }

    public Optional<String> getRawRecord() {
        return rawRecord;
    }

    public void setRawRecord(String rawRecord) {
        this.rawRecord = Optional.fromNullable(Strings.emptyToNull(rawRecord));
    }

    @Override
    public String toString() {
        return "ImportRow{" +
                "lineNumber=" + lineNumber +
                ", bean=" + bean +
                ", errors=" + errors +
                ", next=" + next +
                ", rawRecord=" + rawRecord +
                '}';
    }
}
