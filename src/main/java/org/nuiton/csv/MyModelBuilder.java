/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import java.util.LinkedList;
import java.util.List;

/**
 * @param <E>
 */
public class MyModelBuilder<E> {

    protected char separator;

    protected List<ImportableColumn<E, Object>> columnsForImport = new LinkedList<>();

    protected List<ExportableColumn<E, Object>> columnsForExport = new LinkedList<>();

    public BuildOrAddColumnStep newEmptyModel(char separator) {
        this.separator = separator;
        return new BuildOrAddColumnStep();
    }

    public class BuildOrAddColumnStep<E> {

        public ChooseImportStrategyForColumnStep<E> addColumn(String columnName) {
            return new ChooseImportStrategyForColumnStep(columnName);
        }

        public CsvModel<E> buildModelForExport() {
            return new CsvModel<>(separator, (Iterable) columnsForImport, (Iterable) columnsForExport, null);
        }

        public CsvModel<E> buildModelForImport(CsvModel.Factory<E> emptyEFactory) {
            return new CsvModel<E>(separator, (Iterable) columnsForImport, (Iterable) columnsForExport, (CsvModel.Factory) emptyEFactory);
        }
    }

    public class ChooseImportStrategyForColumnStep<E> {

        protected String columnName;

        public ChooseImportStrategyForColumnStep(String columnName) {
            this.columnName = columnName;
        }

        public ChooseExportStrategyForColumnStep<E> ignoreAtImport() {
            Column<E, Object> importableColumn = Column.newImportableColumn(columnName, null, null, true, false);
            return new ChooseExportStrategyForColumnStep(columnName, importableColumn);
        }

        public ChooseExportStrategyForColumnStep<E> mandatoryAtImport(String propertyName) {
            return mandatoryAtImport(propertyName, Common.STRING);
        }

        public <T> ChooseExportStrategyForColumnStep<E> mandatoryAtImport(String propertyName, ValueParser<T> valueParser) {
            return mandatoryAtImport(valueParser, new Common.BeanProperty<E, T>(propertyName));
        }

        public ChooseExportStrategyForColumnStep<E> mandatoryAtImport(ValueSetter<E, String> valueSetter) {
            return mandatoryAtImport(Common.STRING, valueSetter);
        }

        public <T> ChooseExportStrategyForColumnStep<E> mandatoryAtImport(ValueParser<T> valueParser, ValueSetter<E, T> valueSetter) {
            Column<E, T> importableColumn = Column.newImportableColumn(columnName, valueParser, valueSetter, false, true);
            return new ChooseExportStrategyForColumnStep(columnName, importableColumn);
        }

        public ChooseExportStrategyForColumnStep<E> optionalAtImport(String propertyName) {
            return optionalAtImport(propertyName, Common.STRING);
        }

        public <T> ChooseExportStrategyForColumnStep<E> optionalAtImport(String propertyName, ValueParser<T> valueParser) {
            return optionalAtImport(valueParser, new Common.BeanProperty<E, T>(propertyName));
        }

        public ChooseExportStrategyForColumnStep<E> optionalAtImport(ValueSetter<E, String> valueSetter) {
            return optionalAtImport(Common.STRING, valueSetter);
        }

        public <T> ChooseExportStrategyForColumnStep<E> optionalAtImport(ValueParser<T> valueParser, ValueSetter<E, T> valueSetter) {
            Column<E, T> importableColumn = Column.newImportableColumn(columnName, valueParser, valueSetter, false, true);
            return new ChooseExportStrategyForColumnStep(columnName, importableColumn);
        }

    }

    public class ChooseExportStrategyForColumnStep<E> {

        protected String columnName;

        protected Column<E, Object> importableColumn;

        public ChooseExportStrategyForColumnStep(String columnName, Column<E, Object> importableColumn) {
            this.columnName = columnName;
            this.importableColumn = importableColumn;
        }

        public AddColumnStep<E> ignoredAtExport() {
            Column<E, Object> exportableColumn = Column.newExportableColumn(columnName, null, null, true);
            return new AddColumnStep(importableColumn, exportableColumn);
        }

        public AddColumnStep<E> writeAtExport(String propertyName) {
            return writeAtExport(propertyName, Common.STRING);
        }

        public AddColumnStep<E> writeAtExport(ValueGetter<E, String> valueGetter) {
            return writeAtExport(valueGetter, Common.STRING);
        }

        public <T> AddColumnStep<E> writeAtExport(String propertyName, ValueFormatter<T> valueFormatter) {
            return writeAtExport( new Common.BeanProperty<E, T>(propertyName), valueFormatter);
        }

        public <T> AddColumnStep<E> writeAtExport(ValueGetter<E, T> valueGetter, ValueFormatter<T> valueFormatter) {
            ExportableColumn<E, T> exportableColumn = Column.newExportableColumn(columnName, valueGetter, valueFormatter, false);
            return new AddColumnStep<E>((ImportableColumn) importableColumn, (ExportableColumn) exportableColumn);
        }
    }

    public class AddColumnStep<E> {

        protected ImportableColumn<E, Object> importableColumn;

        protected ExportableColumn<E, Object> exportableColumn;

        public AddColumnStep(ImportableColumn<E, Object> importableColumn, ExportableColumn<E, Object> exportableColumn) {
            this.importableColumn = importableColumn;
            this.exportableColumn = exportableColumn;
        }

        public BuildOrAddColumnStep<E> add() {
            return addIf(true);
        }

        public BuildOrAddColumnStep<E> addIf(boolean condition) {
            if (condition) {
                columnsForImport.add((ImportableColumn) importableColumn);
                columnsForExport.add((ExportableColumn) exportableColumn);
            }
            return new BuildOrAddColumnStep<>();
        }
    }
}
