package org.nuiton.csv;

/*-
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2013 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.nuiton.util.StringUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;

/**
 * Exporter contains a configuration and is ready to export some data.
 *
 * <h2>Exporter construction</h2>
 *
 * <p>To create a new exporter, you need to use an {@link ExporterBuilder}.</p>
 * <p>
 * There is some useful methods such as {@link #of(ExporterConfiguration)} or {@link #of(ExportModel)} to
 * obtain directly an exporter from a legacy export model or a exporter configuration.
 * </p>
 * <p>
 * There is also other method to create a new export builder such as {@link #builder()}, {@link #builder(ExporterConfiguration)} or
 * {@link #builder(ExportModel)}. Once this is done, use the {@link ExporterBuilder} API.
 * </p>
 * <h2>Exporter usage</h2>
 *
 * Once you have an exporter, use one of the method {@code exportTo...(...)} methods.
 *
 * You can export to
 * <ul>
 * <li>File</li>
 * <li>String</li>
 * <li>OutputStream</li>
 * <li>Writer</li>
 * <li>InputStream</li>
 * <li>Reader</li>
 * </ul>
 *
 * Created on 09/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class Exporter<O> {

    /**
     * Creates an exporter from the given model, and using default configuration values.
     *
     * @param model model of export
     * @param <O>   type of data to export
     * @return the exporter ready to use
     */
    public static <O> Exporter<O> of(ExportModel<O> model) {
        return builder(model).build();
    }

    /**
     * Creates an exporter from the given configuration.
     *
     * @param configuration export configuration
     * @param <O>           type of data to export
     * @return the exporter ready to use
     */
    public static <O> Exporter<O> of(ExporterConfiguration<O> configuration) {
        return builder(configuration).build();
    }

    /**
     * Creates a new exporter builder from the given model.
     *
     * @param model model of export
     * @param <O>   type of data to export
     * @return the exporter builder
     */
    public static <O> ExporterBuilder<O> builder(ExportModel<O> model) {
        return Exporter
                .<O>builder()
                .addColumns(model.getColumnsForExport())
                .setCellSeparator(String.valueOf(model.getSeparator()));
    }

    /**
     * Creates a new exporter builder from the given configuration.
     *
     * @param configuration configuration of export
     * @param <O>           type of data to export
     * @return the exporter builder
     */
    public static <O> ExporterBuilder<O> builder(ExporterConfiguration<O> configuration) {
        return new ExporterBuilder<>(configuration);
    }

    /**
     * Creates a new empty exporter builder.
     *
     * @return the exporter builder
     */
    public static <O> ExporterBuilder<O> builder() {
        return new ExporterBuilder<>();
    }

    /**
     * Immutable exporter configuration.
     */
    protected final ExporterConfiguration<O> configuration;

    /**
     * Produces the cvs of the given {@code data} into the given {@code outputStream}.
     *
     * @param data         data to export
     * @param outputStream where to perform the export
     * @throws IOException
     */
    public void toOutputStream(Iterable<O> data, OutputStream outputStream) throws IOException {
        toOutputStream(data, true, outputStream);
    }

    /**
     * Produces the cvs (without header) of the given {@code data} into the given {@code outputStream}.
     *
     * @param data         data to export
     * @param outputStream where to perform the export
     * @throws IOException
     */
    public void toOutputStreamWithoutHeader(Iterable<O> data, OutputStream outputStream) throws IOException {
        toOutputStream(data, false, outputStream);
    }

    /**
     * Produces the cvs of the given {@code data} into the given {@code outputStream}.
     *
     * @param data         data to export
     * @param writeHeader  write or not header (first line of the export)
     * @param outputStream where to perform the export
     * @throws IOException
     */
    public void toOutputStream(Iterable<O> data, boolean writeHeader, OutputStream outputStream) throws IOException {
        ExporterAction<O> action = ExporterAction.of(configuration, data, writeHeader);
        action.export(outputStream);
    }

    /**
     * Produces the cvs of the given {@code data} into the given {@code writer}.
     *
     * @param data   data to export
     * @param writer where to perform the export
     * @throws IOException
     */
    public void toWriter(Iterable<O> data, Writer writer) throws IOException {
        toWriter(data, true, writer);
    }

    /**
     * Produces the cvs (without header) of the given {@code data} into the given {@code writer}.
     *
     * @param data   data to export
     * @param writer where to perform the export
     * @throws IOException
     */
    public void toWriterWithoutHeader(Iterable<O> data, Writer writer) throws IOException {
        toWriter(data, false, writer);
    }

    /**
     * Produces the cvs of the given {@code data} into the given {@code writer}.
     *
     * @param data        data to export
     * @param writeHeader write or not header (first line of the export)
     * @param writer      where to perform the export
     * @throws IOException
     */
    public void toWriter(Iterable<O> data, boolean writeHeader, Writer writer) throws IOException {
        ExporterAction<O> action = ExporterAction.of(configuration, data, writeHeader);
        action.export(writer);
    }

    /**
     * Produces the cvs of the given {@code data} into the given {@code file}.
     *
     * @param data data to export
     * @param file where to perform the export
     * @throws IOException
     */
    public void toFile(Iterable<O> data, File file) throws IOException {
        toFile(data, true, file);
    }

    /**
     * Produces the cvs (without header) of the given {@code data} into the given {@code file}.
     *
     * @param data data to export
     * @param file where to perform the export
     * @throws IOException
     */
    public void toFileWithoutHeader(Iterable<O> data, File file) throws IOException {
        toFile(data, false, file);
    }

    /**
     * Produces the cvs of the given {@code data} into the given {@code file}.
     *
     * @param data        data to export
     * @param writeHeader write or not header (first line of the export)
     * @param file        where to perform the export
     * @throws IOException
     */
    public void toFile(Iterable<O> data, boolean writeHeader, File file) throws IOException {
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            toOutputStream(data, writeHeader, outputStream);
        }
    }

    /**
     * Produces the cvs of the given {@code data} and returns it as a {@link String}.
     *
     * @param data data to export
     * @return the string of the csv
     * @throws IOException
     */
    public String toString(Iterable<O> data) throws IOException {
        String result = toString(data, true);
        return result;
    }

    /**
     * Produces the cvs (without header) of the given {@code data} and returns it as a {@link String}.
     *
     * @param data data to export
     * @return the string of the csv
     * @throws IOException
     */
    public String toStringWithoutHeader(Iterable<O> data) throws IOException {
        String result = toString(data, false);
        return result;
    }

    /**
     * Produces the cvs of the given {@code data} and returns it as a {@link String}.
     *
     * @param data        data to export
     * @param writeHeader write or not header (first line of the export)
     * @return the string of the csv
     * @throws IOException
     */
    public String toString(Iterable<O> data, boolean writeHeader) throws IOException {

        try (StringWriter writer = new StringWriter()) {

            toWriter(data, writeHeader, writer);

            String result = writer.toString();
            return result;

        }

    }

    /**
     * Produces the cvs of the given {@code data} and returns it as a {@link Reader}.
     *
     * @param data data to export
     * @return the string of the csv
     * @throws IOException
     */
    public Reader toReader(Iterable<O> data) throws IOException {
        Reader reader = toReader(data, true);
        return reader;
    }

    /**
     * Produces the cvs (without header) of the given {@code data} and returns it as a {@link Reader}.
     *
     * @param data data to export
     * @return the string of the csv
     * @throws IOException
     */
    public Reader toReaderWithoutHeader(Iterable<O> data) throws IOException {
        Reader reader = toReader(data, false);
        return reader;
    }

    /**
     * Produces the cvs (without header) of the given {@code data} and returns it as a {@link Reader}.
     *
     * @param data        data to export
     * @param writeHeader write or not header (first line of the export)
     * @return the string of the csv
     * @throws IOException
     */
    public Reader toReader(Iterable<O> data, boolean writeHeader) throws IOException {
        ExporterAction<O> action = ExporterAction.of(configuration, data, writeHeader);
        Reader reader = action.toReader();
        return reader;
    }

    /**
     * Produces the cvs of the given {@code data} and returns it as a {@link InputStream}.
     *
     * @param data data to export
     * @return the string of the csv
     * @throws IOException
     */
    public InputStream toInputStream(Iterable<O> data) throws IOException {
        InputStream intputStream = toInputStream(data, true);
        return intputStream;
    }

    /**
     * Produces the cvs (without header) of the given {@code data} and returns it as a {@link InputStream}.
     *
     * @param data data to export
     * @return the string of the csv
     * @throws IOException
     */
    public InputStream toInputStreamWithoutHeader(Iterable<O> data) throws IOException {
        InputStream intputStream = toInputStream(data, false);
        return intputStream;
    }

    /**
     * Produces the cvs (without header) of the given {@code data} and returns it as a {@link InputStream}.
     *
     * @param data data to export
     * @return the string of the csv
     * @throws IOException
     */
    public InputStream toInputStream(Iterable<O> data, boolean writeHeader) throws IOException {
        ExporterAction<O> action = ExporterAction.of(configuration, data, writeHeader);
        InputStream intputStream = action.toInputStream();
        return intputStream;
    }

    protected Exporter(ExporterConfiguration<O> configuration) {
        this.configuration = configuration;
    }

    /**
     * Creates a new exporter builder from this exporter (to reuse his configuration).
     *
     * @return the exporter builder
     */
    public ExporterBuilder<O> toBuilder() {
        return new ExporterBuilder<>(configuration);
    }

    protected static class ExporterAction<E> {

        /**
         * Data to export.
         */
        protected final Iterator<E> dataIterator;

        /**
         * Write header ?
         */
        protected final boolean writeHeader;

        /**
         * Exporter configuration.
         */
        protected final ExporterConfiguration<E> configuration;

        /**
         * Number of cells per row.
         */
        protected final int nbCellsPerRow;

        /**
         * Is the first row was consumed ?
         */
        protected boolean firstRowConsumed;

        /**
         * Current row to export as an array of bytes.
         */
        protected byte[] currentRow;

        /**
         * Position to read in the current row array.
         */
        protected int currentRowPosition;

        protected static <E> ExporterAction<E> of(ExporterConfiguration<E> configuration, Iterable<E> data, boolean writeHeader) {
            return new ExporterAction<>(configuration, writeHeader, data);
        }

        protected ExporterAction(ExporterConfiguration<E> configuration, boolean writeHeader, Iterable<E> data) {
            this.configuration = configuration;
            this.writeHeader = writeHeader;
            this.dataIterator = data.iterator();
            this.nbCellsPerRow = configuration.getColumns().size();
        }

        public void export(OutputStream outputStream) throws IOException {

            for (byte[] row = readFirstRowOrGetCurrentRow(); row != null; row = readNextRow()) {
                outputStream.write(row);
            }

        }

        public void export(Writer writer) throws IOException {

            for (byte[] row = readFirstRowOrGetCurrentRow(); row != null; row = readNextRow()) {
                writer.write(new String(row));
            }

        }

        public InputStream toInputStream() {

            return new InputStream() {

                @Override
                public int read() {

                    byte[] row = readFirstRowOrGetCurrentRow();

                    int result;
                    if (row == null) {

                        // end of stream
                        result = -1;

                    } else {

                        int length = row.length;

                        result = currentRow[currentRowPosition++];

                        if (currentRowPosition >= length) {
                            currentRow = readNextRow();
                        }

                    }

                    return result;

                }

                @Override
                public void close() {
                    // nothing to do
                }

            };
        }

        public Reader toReader() {

            return new InputStreamReader(toInputStream(), configuration.getCharset());

        }

        protected byte[] readFirstRowOrGetCurrentRow() {
            if (!firstRowConsumed) {
                firstRowConsumed = true;
                if (writeHeader) {
                    readHeader();
                } else {
                    readNextRow();
                }
            }
            return currentRow;
        }

        protected byte[] readNextRow() {

            currentRowPosition = 0;
            if (dataIterator.hasNext()) {
                E row = dataIterator.next();
                try {
                    currentRow = readRow(row);
                } catch (Exception e) {
                    throw new ImportRuntimeException("Could not obtain row", e);
                }
            } else {
                currentRow = null;
            }

            return currentRow;

        }

        protected void readHeader() {

            currentRowPosition = 0;
            String cellSeparator = configuration.getCellSeparator();
            StringBuilder rowBuilder = new StringBuilder();
            int index = 0;
            for (ExportableColumn<E, ?> column : configuration.getColumns()) {

                String formattedCell = column.getHeaderName();
                addCellToRowBuilder(rowBuilder, ++index, formattedCell, cellSeparator);

            }
            currentRow = toCurrentRow(rowBuilder);

        }

        protected byte[] readRow(E row) throws Exception {

            String cellSeparator = configuration.getCellSeparator();
            StringBuilder rowBuilder = new StringBuilder();
            int index = 0;
            for (ExportableColumn<E, ?> column : configuration.getColumns()) {

                Object cell = column.getValue(row);
                String formattedCell = ((ExportableColumn) column).formatValue(cell);
                Preconditions.checkNotNull(formattedCell,
                                           String.format("column for header %s (%s) returned a null value.",
                                                         column.getHeaderName(), column.toString()));
                addCellToRowBuilder(rowBuilder, ++index, formattedCell, cellSeparator);

            }
            return toCurrentRow(rowBuilder);

        }

        protected void addCellToRowBuilder(StringBuilder rowBuilder, int index, String formattedCell, String cellSeparator) {
            String csvCell = StringUtil.escapeCsvValue(formattedCell, cellSeparator);
            rowBuilder.append(csvCell);
            if (index < nbCellsPerRow) {
                rowBuilder.append(cellSeparator);
            }
        }

        protected byte[] toCurrentRow(StringBuilder rowBuilder) {
            rowBuilder.append(configuration.getEndOfLineSeparator());
            String formattedRow = rowBuilder.toString();
            return formattedRow.getBytes(configuration.getCharset());
        }

    }

}
