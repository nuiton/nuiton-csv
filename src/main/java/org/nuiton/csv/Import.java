/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 CodeLutin, Tony Chemit, Brendan Le Ny
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.csv;

import com.csvreader.CsvReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ext.CsvReaders;
import org.nuiton.util.StringUtil;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * Import engine for a given import model.
 * <p>
 * It acts as an {@link Iterable}, you can use directly inside a foreach.
 * <p>
 * The method {@link #prepareAndValidate()} will be invoked before all and
 * only once. It mainly obtain header from the csv input, pass it to the model
 * and then validate the model.
 *
 * @author Brendan Le Ny - leny@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @author Florian Desbois
 * @since 2.4
 */
public class Import<E> implements Iterable<E>, Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Import.class);

    /** Csv import model. */
    protected ImportModel<E> model;

    /** Csv reader (this is the input). */
    protected CsvReader reader;

    /**
     * A flag to know if model was already validated.
     * <p>
     * Save once done to prevent multiple headers read leading to consider
     * first lines as headers.
     */
    protected boolean validate;

    public static <E> Import<E> newImport(ImportModel<E> model,
                                          InputStream inputStream) {
        return new Import<>(model, inputStream);
    }

    public static <E> Import<E> newImport(ImportModel<E> model,
                                          Reader reader) {
        return new Import<>(model, reader);
    }

    public static <E> Import<E> newImport(ImportModel<E> model,
                                          InputStream inputStream,
                                          boolean safetySwitch) {
        return new Import<>(model, inputStream, safetySwitch);
    }

    public static <E> Import<E> newImport(ImportModel<E> model,
                                          Reader reader,
                                          boolean safetySwitch) {
        return new Import<>(model, reader, safetySwitch);
    }

    /**
     * Define iterator over import. First of all, the input stream will be
     * validated based on defined model. Iteration will be done on all csv
     * rows except first headers line.
     *
     * @return the Iterator used for csv iteration
     * @see #prepareAndValidate()
     */
    @Override
    public Iterator<E> iterator() {

        prepareAndValidate();

        return new Iterator<E>() {

            // read first line since first line is header
            boolean hasNext = readRow();

            // get once for all columns to import
            List<ImportableColumn<E, Object>> columns = getNonIgnoredHeaders();

            // to stock the current line number
            int lineNumber;

            @Override
            public boolean hasNext() {
                return hasNext;
            }

            @Override
            public E next()
                    throws NoSuchElementException, ImportRuntimeException {

                if (!hasNext) {
                    throw new NoSuchElementException();
                }

                lineNumber += 1;

                E element = model.newEmptyInstance();

                for (ImportableColumn<E, Object> field : columns) {

                    // read value from csv cell
                    String value = readValue(field, lineNumber);

                    // contravariance ftw
                    Object parsedValue = parseValue(field, lineNumber, value);

                    // set value to element
                    setValue(field, lineNumber, element, parsedValue);
                }

                // check if there is a next row to read
                hasNext = readRow();

                return element;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public void close() {
        reader.close();
    }

    /**
     * Read the first mandatory headers line and validate it with the input
     * model. This will check if headers are unique, known by the model and
     * if mandatory headers exist in the file. During this phase, the model
     * will retrieve headers value with {@link
     * ImportModel#pushCsvHeaderNames(List)} call.
     *
     * @since 2.4.1
     */
    public void prepareAndValidate() {

        if (validate) {

            // was already validated
            return;
        }

        // mark as validated
        validate = true;

        // obtains headers
        String[] headers = getHeaders();

        if (log.isTraceEnabled()) {
            log.trace("headers of the CSV file are : " +
                      Arrays.toString(headers));
        }

        // hook to do some stuff from the model
        model.pushCsvHeaderNames(Arrays.asList(headers));

        // check model columns name are unique
        checkUniqueModelColumnNames();

        // check that given headers from csv file are all known
        checkHeaderNamesAreAllKnown(headers);

        // check all mandatories column are on csv header
        checkAllMandatoryHeadersArePresent(headers);
    }

    protected <T> String readValue(ImportableColumn<E, T> field,
                                   int lineNumber) {
        try {
            String value = reader.get(field.getHeaderName());
            return value;
        } catch (Exception e) {
            reader.close();
            throw new ImportRuntimeException(
                    String.format("Unable to read value of column '%s' at line %s",
                      field.getHeaderName(), lineNumber), e);
        }
    }

    protected <T> T parseValue(ImportableColumn<E, T> field,
                               int lineNumber,
                               String value) {
        try {
            T parsedValue = field.parseValue(value);
            return parsedValue;
        } catch (Exception e) {
            String message = String.format("Unable to parse value '%s' (column '%s', line %s)",
                               value, field.getHeaderName(), lineNumber)
                             + "\n" + e.getMessage();
            throw new ImportRuntimeException(message, e);
        }
    }

    protected <T> void setValue(ImportableColumn<E, T> field,
                                int lineNumber,
                                E element,
                                T parsedValue) {
        try {
            field.setValue(element, parsedValue);
        } catch (Exception e) {
            String message = String.format("Unable to set value '%s' (object '%s', line %s, column '%s')",
                               parsedValue,
                               element.toString(),
                               lineNumber, field.getHeaderName());
            if (log.isErrorEnabled()) {
                log.error(message);
            }
            throw new ImportRuntimeException(message, e);
        }
    }

    protected void checkHeaderNamesAreAllKnown(String... headers) {
        List<String> csvHeaders = new ArrayList<>();
        Collections.addAll(csvHeaders, headers);

        for (ImportableColumn<E, ?> field : model.getColumnsForImport()) {
            csvHeaders.remove(field.getHeaderName());
        }
        if (!csvHeaders.isEmpty()) {
            List<String> validHeaderNames = new LinkedList<>();
            for (ImportableColumn<E, ?> importableColumn :
                    model.getColumnsForImport()) {
                validHeaderNames.add(importableColumn.getHeaderName());
            }
            String validationMessage =
                    String.format("Fields %s are not recognized. Accepted fields are %s.",
                      StringUtil.join(csvHeaders, ", ", true),
                      StringUtil.join(validHeaderNames, ", ", true));
            throw new ImportRuntimeException(validationMessage);
        }
    }

    protected void checkUniqueModelColumnNames() {
        Set<String> headerNames = new HashSet<>();
        Set<String> doubleHeaderNames = new HashSet<>();
        for (ImportableColumn<E, ?> importableColumn :
                model.getColumnsForImport()) {
            String headerName = importableColumn.getHeaderName();
            boolean alreadyUsed = !headerNames.add(headerName);
            if (alreadyUsed) {
                doubleHeaderNames.add(headerName);
            }
        }
        if (!doubleHeaderNames.isEmpty()) {
            String message = String.format("Fields %s are duplicated.",
                               StringUtil.join(doubleHeaderNames, ", ", true));

            throw new ImportRuntimeException(
                    message);
        }
    }

    protected void checkAllMandatoryHeadersArePresent(String... headers) {

        List<String> csvHeaders = new ArrayList<>();
        Collections.addAll(csvHeaders, headers);

        List<String> mandatoryHeadersNames = new ArrayList<>();
        for (ImportableColumn<E, ?> field : getAllMandatoryHeaders()) {
            mandatoryHeadersNames.add(field.getHeaderName());
        }
        mandatoryHeadersNames.removeAll(csvHeaders);

        if (!mandatoryHeadersNames.isEmpty()) {
            String validationMessage =
                    String.format("The mandatory fields %s are missing",
                      StringUtil.join(mandatoryHeadersNames, ", ", true));
            throw new ImportRuntimeException(validationMessage);
        }
    }

    protected String[] getHeaders() throws ImportRuntimeException {
        try {
            boolean canReadHeaders = reader.readHeaders();
            if (!canReadHeaders) {
                throw new ImportRuntimeException("Unable to read headers");
            }
        } catch (IOException e) {
            throw new ImportRuntimeException("Unable to read headers", e);
        }

        try {
            String[] result = reader.getHeaders();
            if (result.length > 0) {
                result[0] = CsvReaders.removeBomCharacter(result[0]);
            }
            return result;
        } catch (IOException e) {
            throw new ImportRuntimeException("Unable to read headers", e);
        }
    }

    protected List<ImportableColumn<E, Object>> getNonIgnoredHeaders() {
        List<ImportableColumn<E, Object>> nonIgnoredHeaders =
                new ArrayList<>();
        for (ImportableColumn<E, Object> field : model.getColumnsForImport()) {
            if (!field.isIgnored()) {
                nonIgnoredHeaders.add(field);
            }
        }
        return nonIgnoredHeaders;
    }

    protected List<ImportableColumn<E, ?>> getAllMandatoryHeaders() {
        List<ImportableColumn<E, ?>> allMandatoryHeaders =
                new ArrayList<>();
        for (ImportableColumn<E, ?> field : model.getColumnsForImport()) {
            if (field.isMandatory()) {
                allMandatoryHeaders.add(field);
            }
        }
        return allMandatoryHeaders;
    }

    protected Import(ImportModel<E> model, InputStream inputStream) {
        this(model, inputStream, true);
    }

    protected Import(ImportModel<E> model, Reader reader) {
        this(model, reader, true);
    }

    protected Import(ImportModel<E> model, InputStream inputStream, boolean safetySwitch) {
        if (inputStream == null) {
            throw new NullPointerException("inputStream is null");
        }
        this.model = model;
        reader = new CsvReader(inputStream, model.getSeparator(), Charset.forName("UTF-8"));
        reader.setTrimWhitespace(true);
        this.reader.setSafetySwitch(safetySwitch);
    }

    protected Import(ImportModel<E> model, Reader reader, boolean safetySwitch) {
        if (reader == null) {
            throw new NullPointerException("reader is null");
        }
        this.model = model;
        this.reader = new CsvReader(reader, model.getSeparator());
        this.reader.setTrimWhitespace(true);
        this.reader.setSafetySwitch(safetySwitch);
    }

    /**
     * Read the next row from the reader and return {@code true} if line
     * was successfully read.
     *
     * @return {@code true} if line was successfully read, says in fact there is
     *         something after this line.
     * @throws ImportRuntimeException if could not read line
     */
    protected boolean readRow() throws ImportRuntimeException {
        try {
            boolean hasNext = reader.readRecord();
            return hasNext;
        } catch (IOException e) {
            reader.close();
            throw new ImportRuntimeException(String.format("Unable to read line %s", 1), e);
        }
    }

}
