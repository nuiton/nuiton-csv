package org.nuiton.csv;

/*-
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2013 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * To build an {@link Exporter}.
 *
 * Created on 09/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class ExporterBuilder<O> {

    public static final String DEFAULT_CELL_SEPARATOR = ";";

    public static final String DEFAULT_END_OF_LINE_SEPARATOR = "\n";

    public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    protected final ExporterConfiguration<O> configuration;

    protected final ImmutableSet.Builder<ExportableColumn<O, ?>> columnsBuilder = ImmutableSet.builder();

    /**
     * Set the {@link ExporterConfiguration#cellSeparator} and return the builder.
     *
     * @param cellSeparator new value for {@link ExporterConfiguration#cellSeparator}.
     * @return the builder
     */
    public ExporterBuilder<O> setCellSeparator(String cellSeparator) {
        configuration.setCellSeparator(cellSeparator);
        return this;
    }

    /**
     * Set the {@link ExporterConfiguration#endOfLineSeparator} and return the builder.
     *
     * @param endOfLineSeparator new value for {@link ExporterConfiguration#endOfLineSeparator}.
     * @return the builder
     */
    public ExporterBuilder<O> setEndOfLineSeparator(String endOfLineSeparator) {
        configuration.setEndOfLineSeparator(endOfLineSeparator);
        return this;
    }

    /**
     * Set the {@link ExporterConfiguration#charset} and return the builder.
     *
     * @param charset new value for {@link ExporterConfiguration#charset}.
     * @return the builder
     */
    public ExporterBuilder<O> setCharset(Charset charset) {
        configuration.setCharset(charset);
        return this;
    }

    /**
     * Add the given columns.
     *
     * @param columns columns to add to the builder
     * @return the builder
     */
    public ExporterBuilder<O> addColumns(Iterable<ExportableColumn<O, Object>> columns) {
        columnsBuilder.addAll(columns);
        return this;
    }

    /**
     * Add a new column for the given property name, using also the property name as the export header name.
     *
     * The property is exported using a String formatted.
     *
     * @param propertyName the property name to export
     * @return the builder
     */
    public ExporterBuilder<O> addColumn(String propertyName) {
        return addColumn(propertyName, propertyName, Common.STRING);
    }

    /**
     * Add a new column for the given property name and export as the given header name.
     *
     * The property is obtained using a simple getter and exported using a String formatter.
     *
     * @param headerName   the header name to use in export
     * @param propertyName the property name to export
     * @return the builder
     */
    public ExporterBuilder<O> addColumn(String headerName, String propertyName) {
        return addColumn(headerName, propertyName, Common.STRING);
    }

    /**
     * Add a new column for the given property name and export as the given header name.
     *
     * The property is obtained using the given value getter and exported using a String formatter.
     *
     * @param headerName  the header name to use in export
     * @param valueGetter the value getter
     * @return the builder
     */
    public ExporterBuilder<O> addColumn(String headerName, ValueGetter<O, String> valueGetter) {
        return addColumn(headerName, valueGetter, Common.STRING);
    }

    /**
     * Add a new column for the given property name and export as the given header name.
     *
     * The property is obtained using a simple getter and exported using the given value formatter.
     *
     * @param headerName     the header name to use in export
     * @param propertyName   the property name to export
     * @param valueFormatter the value formatter
     * @return the builder
     */
    public <T> ExporterBuilder<O> addColumn(String headerName, String propertyName, ValueFormatter<T> valueFormatter) {
        return addColumn(headerName, new Common.BeanProperty<O, T>(propertyName), valueFormatter);
    }

    /**
     * Add a new column for the given property name and exported as the given header name.
     *
     * The property is obtained using the value getter and exported using the given value formatter.
     *
     * @param headerName     the header name to use in export
     * @param valueGetter    the value getter
     * @param valueFormatter the value formatter
     * @return the builder
     */
    public <T> ExporterBuilder<O> addColumn(String headerName, ValueGetter<O, T> valueGetter, ValueFormatter<T> valueFormatter) {
        ExportableColumn<O, T> newColumn = Column.newExportableColumn(headerName, valueGetter, valueFormatter, false);
        columnsBuilder.add(newColumn);
        return this;
    }

    public Exporter<O> build() {
        configuration.setColumns(columnsBuilder.build());
        Preconditions.checkNotNull(configuration.getColumns(), "No columns defined.");
        Preconditions.checkNotNull(configuration.getCellSeparator(), "No charset defined.");
        Preconditions.checkNotNull(configuration.getCellSeparator(), "No cellSeparator defined.");
        Preconditions.checkNotNull(configuration.getEndOfLineSeparator(), "No endOfLineSeparator defined.");

        return new Exporter<>(configuration);
    }

    public ExporterBuilder() {
        this.configuration = new ExporterConfiguration<>();
        setCharset(DEFAULT_CHARSET);
        setCellSeparator(DEFAULT_CELL_SEPARATOR);
        setEndOfLineSeparator(DEFAULT_END_OF_LINE_SEPARATOR);
    }

    public ExporterBuilder(ExporterConfiguration<O> configuration) {
        this.configuration = new ExporterConfiguration<>();
        addColumns((Iterable) configuration.getColumns());
        setCharset(configuration.getCharset());
        setCellSeparator(configuration.getCellSeparator());
        setEndOfLineSeparator(configuration.getEndOfLineSeparator());
    }

}
