package org.nuiton.csv.ext;
/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ImportExportModel;
import org.nuiton.csv.ImportableColumn;
import org.nuiton.csv.ImportableExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueGetter;
import org.nuiton.csv.ValueGetterSetter;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ValueParserFormatter;
import org.nuiton.csv.ValueSetter;

import java.util.List;

/**
 * Abstract implementation of a {@link ImportExportModel} to avoid all the
 * boilerplate code when creating a new model.
 *
 * @param <E> type of object to import/export
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public abstract class AbstractImportExportModel<E> implements ImportExportModel<E> {

    private final char separator;

    protected final ModelBuilder<E> modelBuilder;

    public AbstractImportExportModel(char separator) {
        this.separator = separator;
        modelBuilder = new ModelBuilder<>();
    }

    @Override
    public final char getSeparator() {
        return separator;
    }

    @Override
    public void pushCsvHeaderNames(List<String> headerNames) {
    }

    @Override
    public final Iterable<ExportableColumn<E, Object>> getColumnsForExport() {
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public final Iterable<ImportableColumn<E, Object>> getColumnsForImport() {
        return (Iterable) modelBuilder.getColumnsForImport();
    }

    public <T> ImportableColumn<E, T> newIgnoredColumn(String headerName) {
        return modelBuilder.newIgnoredColumn(headerName);
    }

    public ImportableColumn<E, String> newMandatoryColumn(String headerName) {
        return modelBuilder.newMandatoryColumn(headerName, headerName);
    }

    public ImportableColumn<E, String> newMandatoryColumn(String headerName, String propertyName) {
        return modelBuilder.newMandatoryColumn(headerName, propertyName);
    }

    public <T> ImportableColumn<E, T> newMandatoryColumn(String headerName, ValueParser<T> valueParser) {
        return modelBuilder.newMandatoryColumn(headerName, headerName, valueParser);
    }

    public <T> ImportableColumn<E, T> newMandatoryColumn(String headerName, String propertyName, ValueParser<T> valueParser) {
        return modelBuilder.newMandatoryColumn(headerName, propertyName, valueParser);
    }

    public ImportableColumn<E, String> newMandatoryColumn(String headerName, ValueSetter<E, String> eStringValueSetter) {
        return modelBuilder.newMandatoryColumn(headerName, eStringValueSetter);
    }

    public <T> ImportableColumn<E, T> newMandatoryColumn(String headerName, ValueParser<T> valueParser, ValueSetter<E, T> etValueSetter) {
        return modelBuilder.newMandatoryColumn(headerName, valueParser, etValueSetter);
    }

    public ImportableColumn<E, String> newOptionalColumn(String headerName) {
        return modelBuilder.newOptionalColumn(headerName, headerName);
    }

    public ImportableColumn<E, String> newOptionalColumn(String headerName, String propertyName) {
        return modelBuilder.newOptionalColumn(headerName, propertyName);
    }

    public <T> ImportableColumn<E, T> newOptionalColumn(String headerName, ValueParser<T> valueParser) {
        return modelBuilder.newOptionalColumn(headerName, headerName, valueParser);
    }

    public <T> ImportableColumn<E, T> newOptionalColumn(String headerName, String propertyName, ValueParser<T> valueParser) {
        return modelBuilder.newOptionalColumn(headerName, propertyName, valueParser);
    }

    public ImportableColumn<E, String> newOptionalColumn(String headerName, ValueSetter<E, String> eStringValueSetter) {
        return modelBuilder.newOptionalColumn(headerName, eStringValueSetter);
    }

    public <T> ImportableColumn<E, T> newOptionalColumn(String headerName, ValueParser<T> valueParser, ValueSetter<E, T> etValueSetter) {
        return modelBuilder.newOptionalColumn(headerName, valueParser, etValueSetter);
    }

    public ExportableColumn<E, String> newColumnForExport(String headerName) {
        return modelBuilder.newColumnForExport(headerName, headerName);
    }

    public ExportableColumn<E, String> newColumnForExport(String headerName, String propertyName) {
        return modelBuilder.newColumnForExport(headerName, propertyName);
    }

    public ExportableColumn<E, String> newColumnForExport(String headerName, ValueGetter<E, String> eStringValueGetter) {
        return modelBuilder.newColumnForExport(headerName, eStringValueGetter);
    }

    public <T> ExportableColumn<E, T> newColumnForExport(String headerName, ValueFormatter<T> valueFormatter) {
        return modelBuilder.newColumnForExport(headerName, headerName, valueFormatter);
    }

    public <T> ExportableColumn<E, T> newColumnForExport(String headerName, String propertyName, ValueFormatter<T> valueFormatter) {
        return modelBuilder.newColumnForExport(headerName, propertyName, valueFormatter);
    }

    public <T> ExportableColumn<E, T> newColumnForExport(String headerName, ValueGetter<E, T> etValueGetter, ValueFormatter<T> valueFormatter) {
        return modelBuilder.newColumnForExport(headerName, etValueGetter, valueFormatter);
    }

    public ImportableExportableColumn<E, String> newColumnForImportExport(String headerName) {
        return modelBuilder.newColumnForImportExport(headerName, headerName);
    }

    public ImportableExportableColumn<E, String> newColumnForImportExport(String headerName, String propertyName) {
        return modelBuilder.newColumnForImportExport(headerName, propertyName);
    }

    public ImportableExportableColumn<E, String> newColumnForImportExport(String headerName, ValueGetterSetter<E, String> eStringValueGetterSetter) {
        return modelBuilder.newColumnForImportExport(headerName, eStringValueGetterSetter);
    }

    public <T> ImportableExportableColumn<E, T> newColumnForImportExport(String headerName, ValueParserFormatter<T> valueParserFormatter) {
        return modelBuilder.newColumnForImportExport(headerName, headerName, valueParserFormatter);
    }

    public <T> ImportableExportableColumn<E, T> newColumnForImportExport(String headerName, String propertyName, ValueParserFormatter<T> valueParserFormatter) {
        return modelBuilder.newColumnForImportExport(headerName, propertyName, valueParserFormatter);
    }

    public <T> ImportableExportableColumn<E, T> newColumnForImportExport(String headerName, ValueGetterSetter<E, T> etValueGetterSetter, ValueParserFormatter<T> valueParserFormatter) {
        return modelBuilder.newColumnForImportExport(headerName, etValueGetterSetter, valueParserFormatter);
    }
}
