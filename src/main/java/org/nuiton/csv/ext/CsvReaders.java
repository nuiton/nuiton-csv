package org.nuiton.csv.ext;
/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.nuiton.csv.ImportRuntimeException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

/**
 * Useful method around csv readers.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class CsvReaders {

    /**
     * BOM is a character you can find in the beginning of a UTF-8 encoded file that should not be interpreted.
     *
     * See http://en.wikipedia.org/wiki/Byte_order_mark
     */
    protected static final char BOM_CHARACTER = '\uFEFF';

    public static String[] getHeader(File file, char charSeprator) {

        try {
            BufferedReader reader = Files.newReader(file, Charsets.UTF_8);
            try {
                String header = reader.readLine();
                String[] result = header.split(charSeprator + "");
                return result;
            } finally {
                reader.close();
            }
        } catch (IOException e) {
            throw new ImportRuntimeException("Could not obtain header of file " + file, e);
        }
    }

    public static String removeBomCharacter(String s) {
        return s.replaceFirst(BOM_CHARACTER + "?", "");
    }

}
