package org.nuiton.csv.ext;
/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.csv.Export;
import org.nuiton.csv.ExportModel;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;

/**
 * Extends the {@link Export} classes to be able to generate only once
 * the header.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class RepeatableExport<E> extends Export<E> {

    public static <E> RepeatableExport<E> newExport(ExportModel<E> model,
                                                    Iterable<E> data,
                                                    boolean writeOnceHeader) {
        return new RepeatableExport<>(model, data, writeOnceHeader);
    }

    public static <E> void exportToWriter(ExportModel<E> model,
                                          Iterable<E> data,
                                          Writer writer,
                                          boolean writeOnceHeader) throws Exception {
        Export<E> exporter = newExport(model, data, writeOnceHeader);
        exporter.write(writer);
    }

    public static <E> void exportToFile(ExportModel<E> model,
                                        Iterable<E> data,
                                        File file,
                                        Charset charset,
                                        boolean writeOnceHeader) throws Exception {
        Export<E> exporter = newExport(model, data, writeOnceHeader);
        exporter.write(file, charset);
    }

    public static <E> String exportToString(ExportModel<E> model,
                                            Iterable<E> data,
                                            Charset charset,
                                            boolean writeOnceHeader) throws Exception {
        Export<E> exporter = newExport(model, data, writeOnceHeader);
        return exporter.toString(charset);
    }

    protected final boolean writeOnceHeader;

    protected boolean headerWritten;

    public boolean isHeaderWritten() {
        return headerWritten;
    }

    protected RepeatableExport(ExportModel<E> model,
                               Iterable<E> data,
                               boolean writeOnceHeader) {
        super(model, data);
        this.writeOnceHeader = writeOnceHeader;
    }

    @Override
    protected void writeHeader(Writer writer) throws IOException {
        if (!writeOnceHeader || !headerWritten) {

            // no header generated, let's do it!

            super.writeHeader(writer);

            // mark it as written
            headerWritten = true;
        }
    }
}
