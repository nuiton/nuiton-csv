package org.nuiton.csv.ext;
/*
 * #%L
 * Nuiton CSV
 * %%
 * Copyright (C) 2011 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueGetter;

/**
 * Abstract implementation of a {@link ExportModel} to avoid all the
 * boilerplate code when creating a new model.
 *
 * @param <E> type of object to export
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public abstract class AbstractExportModel<E> implements ExportModel<E> {

    private final char separator;

    protected final ModelBuilder<E> modelBuilder;

    public AbstractExportModel(char separator) {
        this.separator = separator;
        modelBuilder = new ModelBuilder<>();
    }

    @Override
    public final char getSeparator() {
        return separator;
    }

    @Override
    public final Iterable<ExportableColumn<E, Object>> getColumnsForExport() {
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    public ExportableColumn<E, String> newColumnForExport(String headerName) {
        return modelBuilder.newColumnForExport(headerName, headerName);
    }

    public ExportableColumn<E, String> newColumnForExport(String headerName, String propertyName) {
        return modelBuilder.newColumnForExport(headerName, propertyName);
    }

    public ExportableColumn<E, String> newColumnForExport(String headerName, ValueGetter<E, String> eStringValueGetter) {
        return modelBuilder.newColumnForExport(headerName, eStringValueGetter);
    }

    public <T> ExportableColumn<E, T> newColumnForExport(String headerName, ValueFormatter<T> valueFormatter) {
        return modelBuilder.newColumnForExport(headerName, headerName, valueFormatter);
    }

    public <T> ExportableColumn<E, T> newColumnForExport(String headerName, String propertyName, ValueFormatter<T> valueFormatter) {
        return modelBuilder.newColumnForExport(headerName, propertyName, valueFormatter);
    }

    public <T> ExportableColumn<E, T> newColumnForExport(String headerName, ValueGetter<E, T> etValueGetter, ValueFormatter<T> valueFormatter) {
        return modelBuilder.newColumnForExport(headerName, etValueGetter, valueFormatter);
    }
}
